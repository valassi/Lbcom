
//-----------------------------------------------------------------------------
/** @file RichDetParameters.cpp
 *
 * Implementation file for class : Rich::DetParameters
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 14/01/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichDetParameters.h"

DECLARE_COMPONENT( Rich::DetParameters )

// General Rich namespace
namespace Rich
{

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  DetParameters::DetParameters( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
    : Rich::ToolBase ( type, name , parent  )
  {
    declareInterface<IDetParameters>(this);
    //                                                    Aero      R1Gas     R2Gas
    declareProperty( "MaxPhotonEnergy",  m_maxPhotEn  = { 4.0,      7.0,      7.0      } );
    declareProperty( "MinPhotonEnergy",  m_minPhotEn  = { 1.75,     1.75,     1.75     } );
    declareProperty( "MeanPhotonEnergy", m_meanPhotEn = { 2.89,     4.29,     4.34     } );
    declareProperty( "RefIndexSD",            m_refSD = { 0.488e-3, 0.393e-4, 0.123e-4 } );
  }

  StatusCode DetParameters::initialize()
  {
    // Initialise base class
    const StatusCode sc = Rich::ToolBase::initialize();
    if ( sc.isFailure() ) return sc;

    // Initialise the data. Eventually, some of this should come from a DataBase

    // Out radiator limits for a single HPD panel (unsigned)
    m_radOutLimLoc = {
      RadLimits ( 0,   625,  0,   600 ), // Aerogel
      RadLimits ( 0,   625,  180, 600 ), // R1Gas
      RadLimits ( 50,  690,  0,   696 )  // R2Gas
    }; 

    return sc;
  }

  double DetParameters::maxPhotonEnergy( const Rich::RadiatorType rad ) const
  {
    return m_maxPhotEn[rad];
  }

  double DetParameters::minPhotonEnergy( const Rich::RadiatorType rad ) const
  {
    return m_minPhotEn[rad];
  }

  double DetParameters::meanPhotonEnergy( const Rich::RadiatorType rad ) const
  {
    return m_meanPhotEn[rad];
  }

  const IDetParameters::RadLimits &
  Rich::DetParameters::AvAcceptOuterLimitsLocal( const Rich::RadiatorType rad ) const
  {
    return m_radOutLimLoc[rad];
  }

  double DetParameters::refIndexSD( const Rich::RadiatorType rad ) const
  {
    return m_refSD[rad];
  }

}
