
//-----------------------------------------------------------------------------
/** @file RichHPDAnalysisAlg.cpp
 *
 *  Implementation file for monitor : Rich::DAQ::HPDAnalysisAlg
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2008-10-14
 */
//-----------------------------------------------------------------------------

// local
#include "RichHPDAnalysisAlg.h"

//-----------------------------------------------------------------------------

using namespace Rich;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HPDAnalysisAlg )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HPDAnalysisAlg::HPDAnalysisAlg( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : Rich::AlgBase ( name , pSvcLocator )
{
  declareProperty( "RawEventLocations", m_taeEvents = { "" } );
  declareProperty( "HPDAnalysisTools",  m_toolNames );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HPDAnalysisAlg::initialize()
{
  const StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // get decoder tool
  acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, nullptr, true );

  // HPD tools
  if ( m_toolNames.empty() ) return Warning( "No HPD analysis tools configured" );
  info() << "Loading HPD Analysis tools : " << m_toolNames << endmsg;
  for ( const auto& t : m_toolNames )
  {
    const Rich::IGenericHPDAnalysisTool * tool(nullptr);
    acquireTool( t, tool, this );
    m_tools.push_back(tool);
  }

  // return
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HPDAnalysisAlg::execute()
{
  StatusCode sc = StatusCode::SUCCESS;

  for ( const auto& evt : m_taeEvents ) { if ( sc ) sc = processTAEEvent(evt); }

  return sc;
}

StatusCode HPDAnalysisAlg::processTAEEvent( const std::string & taeEvent )
{
  // get the decoded data for this tae event
  const auto & l1Map = m_SmartIDDecoder->allRichSmartIDs(taeEvent);

  for ( const auto& L1 : l1Map )
  {
    for ( const auto& ingress : L1.second )
    {
      for ( const auto& hpd : ingress.second.pdData() )
      {
        // HPD ID
        const auto & pdID = hpd.second.pdID();

        // Vector of SmartIDs
        const auto & rawIDs = hpd.second.smartIDs();

        // Loop over HPD tools
        for ( const auto* t : m_tools )
        {
          // Analysis the HPD data
          IGenericHPDAnalysisTool::Results results;
          const auto sc = t->analyse( pdID, rawIDs, results );
          if ( !sc )
          {
            Warning( "HPD Analysis failed", sc ).ignore();
          }
          else
          {
            // count any results
            for ( const auto& res : results )
            {
              if ( res.status.isSuccess() ) { ++counter( res.message ); }
              else
              { Warning( res.message, StatusCode::SUCCESS, 5 ).ignore(); }
            }
          }
        } // tools loop

      }
    }
  }

  return StatusCode::SUCCESS;
}
