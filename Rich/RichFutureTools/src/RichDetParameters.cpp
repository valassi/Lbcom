
//-----------------------------------------------------------------------------
/** @file RichDetParameters.cpp
 *
 * Implementation file for class : Rich::DetParameters
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 14/01/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichDetParameters.h"

using namespace Rich::Future;

DECLARE_COMPONENT( DetParameters )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DetParameters::DetParameters( const std::string& type,
                              const std::string& name,
                              const IInterface* parent )
: ToolBase ( type, name , parent  )
{
  declareInterface<IDetParameters>(this);
}

StatusCode DetParameters::initialize()
{
  // Initialise base class
  const auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  // Initialise the data. Eventually, some of this should come from a DataBase

  // Out radiator limits for a single HPD panel (unsigned)
  m_radOutLimLoc = {
    RadLimits ( 0,   625,  0,   600 ), // Aerogel
    RadLimits ( 0,   625,  180, 600 ), // R1Gas
    RadLimits ( 50,  690,  0,   696 )  // R2Gas
  };

  return sc;
}

double DetParameters::maxPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_maxPhotEn[rad];
}

double DetParameters::minPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_minPhotEn[rad];
}

double DetParameters::meanPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_meanPhotEn[rad];
}

const Rich::IDetParameters::RadLimits &
DetParameters::AvAcceptOuterLimitsLocal( const Rich::RadiatorType rad ) const
{
  return m_radOutLimLoc[rad];
}

double DetParameters::refIndexSD( const Rich::RadiatorType rad ) const
{
  return m_refSD[rad];
}
