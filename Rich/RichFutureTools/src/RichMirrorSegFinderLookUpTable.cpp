
//-----------------------------------------------------------------------------
/** @file RichMirrorSegFinderLookUpTable.cpp
 *
 *  Implementation file for class : Rich::MirrorSegFinderLookUpTable
 *
 *  @author Chris Jones
 *  @date   2015-02-01
 */
//----------------------------------------------------------------------------

// local
#include "RichMirrorSegFinderLookUpTable.h"

using namespace Rich::Future;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MirrorSegFinderLookUpTable::MirrorSegFinderLookUpTable( const std::string& type,
                                                        const std::string& name,
                                                        const IInterface* parent )
  : ToolBase ( type, name, parent )
{
  // define interfaces
  declareInterface<IMirrorSegFinder>(this);
  declareInterface<IMirrorSegFinderLookUpTable>(this);
  //setProperty( "OutputLevel", MSG::DEBUG );
}

//=========================================================================
//  initialization
//=========================================================================
StatusCode MirrorSegFinderLookUpTable::initialize( )
{
  // intialise base class
  auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  // Force an update of the cached mirror information
  // Also sets up the UMS service for future updates as needed
  if ( sc ) sc = mirrorUpdate();

  // return
  return sc;
}

//=========================================================================
//  finalize
//=========================================================================
StatusCode MirrorSegFinderLookUpTable::finalize( )
{
  // unregister from UMS
  updMgrSvc()->unregister(this);

  return ToolBase::finalize();
}

//=========================================================================
// Update Mirror information on Condition changes
//=========================================================================
StatusCode MirrorSegFinderLookUpTable::mirrorUpdate()
{
  _ri_debug << "Mirror Update Triggered" << endmsg;

  // reset the mirror finder objects
  m_sphMirrFinder.reset();
  m_secMirrFinder.reset();

  // get all the mirror segments
  loadMirrors( Rich::Rich1 );
  loadMirrors( Rich::Rich2 );

  // Flag the first update as having been done
  m_firstUpdate = false;

  // Initialise the finders
  _ri_debug << "Initialising the mirror finders" << endmsg;
  m_sphMirrFinder.init();
  m_secMirrFinder.init();

  // debug printout
  printMirrors();

  return ( m_sphMirrFinder.isOK() &&
           m_secMirrFinder.isOK() ? StatusCode::SUCCESS :
           Error("Problem initialising mirror finders") );
}

//=========================================================================
// Load the mirrors from the DB
//=========================================================================
void MirrorSegFinderLookUpTable::loadMirrors( const Rich::DetectorType rich )
{
  // Load the RICH detector element
  const auto * deRich = getDet<DeRich>( DeRichLocations::location(rich) );

  // load the primary mirrors
  for ( const auto& loc : deRich->paramVect<std::string>("SphericalMirrorDetElemLocations") )
  {
    auto * m = getDet<DeRichSphMirror>(loc);
    m_sphMirrFinder.addMirror( rich, side(m,rich), m );
    if ( m_firstUpdate )
    { updMgrSvc()->registerCondition( this, m, &MirrorSegFinderLookUpTable::mirrorUpdate ); }
  }

  // secondary mirrors
  for ( const auto& loc : deRich->paramVect<std::string>("SecondaryMirrorDetElemLocations") )
  {
    auto * m = getDet<DeRichSphMirror>(loc);
    m_secMirrFinder.addMirror( rich, side(m,rich), m );
    if ( m_firstUpdate )
    { updMgrSvc()->registerCondition( this, m, &MirrorSegFinderLookUpTable::mirrorUpdate ); }
  }
}

//=========================================================================
// Find spherical mirror segment and return pointer
//=========================================================================
const DeRichSphMirror*
MirrorSegFinderLookUpTable::
findSphMirror( const Rich::DetectorType rich,
               const Rich::Side side,
               const Gaudi::XYZPoint& reflPoint ) const
{
  // Find the mirror from the lookup map
  return m_sphMirrFinder.find( rich, side, reflPoint );
}

//=========================================================================
// Find secondary mirror segment and return pointer
//=========================================================================
const DeRichSphMirror*
MirrorSegFinderLookUpTable::
findSecMirror( const Rich::DetectorType rich,
               const Rich::Side side,
               const Gaudi::XYZPoint& reflPoint ) const
{
  // Find the mirror from the lookup map
  return m_secMirrFinder.find( rich, side, reflPoint );
}

//=========================================================================
// Debug printout of the mirrors
//=========================================================================
void MirrorSegFinderLookUpTable::printMirrors() const
{
  if ( msgLevel(MSG::DEBUG) )
  {
    for ( const auto& D : { std::make_pair(Rich::Rich1,Rich::top),
                            std::make_pair(Rich::Rich1,Rich::bottom),
                            std::make_pair(Rich::Rich2,Rich::left),
                            std::make_pair(Rich::Rich2,Rich::right) } )
    {

      const auto sphMirrs = m_sphMirrFinder.mirrors( D.first, D.second );
      debug() << "Found " << sphMirrs.size()
              << " primary mirrors for " << D.first << " "
              << Rich::text(D.first,D.second) << " :";
      for ( const auto M : sphMirrs )
      { debug() << " " << M->mirrorNumber(); }
      debug() << endmsg;
      if ( msgLevel(MSG::VERBOSE) )
      {
        for ( const auto M : sphMirrs )
        {
          verbose() << " -> Spherical mirror " << M->mirrorNumber()
                    << " " << M->name()
                    << " " << M->mirrorCentre()
                    << endmsg;
        }
      }

      const auto secMirrs = m_secMirrFinder.mirrors( D.first, D.second );
      debug() << "Found " << secMirrs.size()
              << " secondary mirrors for " << D.first << " "
              << Rich::text(D.first,D.second) << " :";
      for ( const auto M : secMirrs )
      { debug() << " " << M->mirrorNumber(); }
      debug() << endmsg;
      if ( msgLevel(MSG::VERBOSE) )
      {
        for ( const auto M : secMirrs )
        {
          verbose() << " -> Secondary mirror " << M->mirrorNumber()
                    << " " << M->name()
                    << " " << M->mirrorCentre()
                    << endmsg;
        }
      }

    }
  }
}

//=========================================================================

DECLARE_COMPONENT( MirrorSegFinderLookUpTable )

//=========================================================================
