
//-----------------------------------------------------------------------------
/** @file RichHighOccHPDSuppressionTool.h
 *
 *  Header file for tool : Rich::Future::DAQ::HighOccHPDSuppressionTool
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// STD
#include <sstream>

// from Gaudi
#include "GaudiKernel/IToolSvc.h"

// base class
#include "RichFutureKernel/RichToolBase.h"

// Interfaces
#include "RichInterfaces/IRichPixelSuppressionTool.h"

// numberings
#include "RichUtils/RichDAQDefinitions.h"

// RichDet
#include "RichDet/DeRichSystem.h"

namespace Rich
{
  namespace Future
  {
    namespace DAQ
    {

      //-----------------------------------------------------------------------------
      /** @class HighOccHPDSuppressionTool RichHighOccHPDSuppressionTool.h
       *
       *  Tool for monitoring high occupancy HPDs
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   21/03/2006
       */
      //-----------------------------------------------------------------------------

      class HighOccHPDSuppressionTool : public Rich::Future::ToolBase,
                                        virtual public Rich::DAQ::IPixelSuppressionTool
      {

      public: // Methods for Gaudi Framework

        /// Standard constructor
        HighOccHPDSuppressionTool( const std::string& type,
                                   const std::string& name,
                                   const IInterface* parent );

      public: // methods (and doxygen comments) inherited from interface

        // Monitor the occupancy of the given HPD
        bool applyPixelSuppression( const LHCb::RichSmartID hpdID,
                                    LHCb::RichSmartID::Vector & smartIDs ) const override;

      private: // private data

        // absolute max HPD occupancy
        Gaudi::Property<unsigned int> m_overallMax { this, "AbsoluteMaxHPDOcc", 200 };

      protected:

        /// Print summary of suppressions each event ?
        Gaudi::Property<bool> m_sumPrint { this, "PrintHPDSuppressions", true };

      };

    }
  }
}
