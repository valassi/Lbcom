
#pragma once

// STL
#include <string>
#include <memory>
#include <algorithm>
#include <functional>
#include <limits>

// Gaudi ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Rich (Future) Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSmartIDSorter.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Boost
#include "boost/optional.hpp"

namespace Rich
{
  namespace Future
  {

    // Use the functional framework
    using namespace Gaudi::Functional;

    /** @class SmartIDClustering RichSmartIDClustering.h
     *
     *  Creates Rich PD cluster objects from the decoded RICH raw event
     *
     *  @author Chris Jones
     *  @date   2016-09-27
     */
    class SmartIDClustering final :
      public Transformer< Rich::PDPixelCluster::Vector( const Rich::Future::DAQ::L1Map& ),
                          Traits::BaseClass_t<AlgBase> >
    {
      
    public:

      /// Standard constructor
      SmartIDClustering( const std::string& name, ISvcLocator* pSvcLocator );

      /// Initialize
      StatusCode initialize() override;

      /// Algorithm execution via transform
      Rich::PDPixelCluster::Vector operator()( const Rich::Future::DAQ::L1Map& data ) const override;
      
    private:
      
      /** @class SortALICEIDs RichSmartIDClustering.h
       *  Class to sort ALICE RichSmartIDs into the correct order for cluster finding
       */
      class SortALICEIDs
        : std::binary_function< const LHCb::RichSmartID, const LHCb::RichSmartID, bool >
      {

      public:

        /** Sort operator for the RichSmartIDs
         *
         *  Sorts ALICE mode pixels into the correct order for cluster finding
         *
         *  @param p1 First RichSmartID
         *  @param p2 Second RichSmartID
         *
         *  @return bool indicating if p1 should be listed before p2
         */
        inline bool operator() ( const LHCb::RichSmartID& p1, const LHCb::RichSmartID& p2 ) const
        {
          return ( 100000*((Rich::DAQ::NumAlicePixelsPerLHCbPixel*p1.pixelRow())+p1.pixelSubRow()) + p1.pixelCol() <
                   100000*((Rich::DAQ::NumAlicePixelsPerLHCbPixel*p2.pixelRow())+p2.pixelSubRow()) + p2.pixelCol() );
        }

      };

    private:

      /** Check the status of the given RICH PD identifier (RichSmartID)
       *
       *  @param id The PD RichSmartID to check
       *
       *  @return boolean indicating if the given channel is active
       *  @retval true  PD is active
       *  @retval false PD is not in use
       */
      inline bool pdIsOK( const LHCb::RichSmartID id ) const
      {
        return ( id.isValid() &&                             // Valid PD ID
                 m_usedDets[id.rich()] &&                    // This RICH is in use
                 ( !m_pdCheck || m_richSys->pdIsActive(id) ) // If required, check PD is alive
          );
      }

      /// Sort the RichSmartIDs into the correct order for this algorithm
      inline void sortIDs( LHCb::RichSmartID::Vector & smartIDs ) const
      {
        if ( !smartIDs.empty() )
        {
          if ( UNLIKELY( smartIDs.front().pixelSubRowDataIsValid() ) )
          {
            // use own method for ALICE mode
            std::stable_sort( smartIDs.begin(), smartIDs.end(), SortALICEIDs() );
          }
          else
          {
            // LHCb mode is OK to use (faster) helper class
            SmartIDSorter::sortByRegion(smartIDs);
          }
        }
      }

    private:

      /// Pointer to RICH system detector element
      const DeRichSystem * m_richSys = nullptr;

      /// Flags for which RICH detectors to create pixels for
      Gaudi::Property< DetectorArray<bool> > m_usedDets
      { this, "Detectors", { true, true } };

      /// Flag to turn on or off the explicit checking of the PD status
      Gaudi::Property<bool> m_pdCheck
      { this, "CheckPDsAreActive", false };

      /** Turn on/off the clustering for each RICH. 
       *  If off, a single cluster is made from each channel ID */
      Gaudi::Property< DetectorArray<bool> > m_clusterHits
      { this, "ApplyPixelClustering", { false, false } };

      /// Minimum cluster size
      Gaudi::Property<unsigned int> m_minClusSize
      { this, "MinClusterSize", 1u };

      /// Maximum cluster size
      Gaudi::Property<unsigned int> m_maxClusSize 
      { this, "MaxClusterSize", 999999u };

      /// Allow pixels to be clustered across diagonals
      Gaudi::Property<bool> m_allowDiags 
      { this, "AllowDiagonalsInClusters", true };

      /// Max pixels tracks GEC
      Gaudi::Property<unsigned int> m_maxClusters 
      { this, "MaxClusters", std::numeric_limits<unsigned int>::max() };

      /// Absolute max HPD occupancy. If exceeded PD is rejected.
      Gaudi::Property< DetectorArray<std::size_t> > m_overallMax 
      { this, "AbsoluteMaxHPDOcc", { 200, 200 } };

    private:

      /// Flag to turn on the option to split clusters
      bool m_splitClusters = false;

    };

  }
}
