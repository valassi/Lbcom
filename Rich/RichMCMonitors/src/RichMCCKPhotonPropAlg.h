
//-----------------------------------------------------------------------------
/** @file RichMCCKPhotonPropAlg.h
 *
 * Header file for monitor algorithm Rich::MC::MCCKPhotonPropAlg
 *
 * CVS Log :-
 * $Id: RichMCCKPhotonPropAlg.h,v 1.3 2008-05-08 12:36:28 jonrob Exp $
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-03
 */
//-----------------------------------------------------------------------------

#ifndef RICHMCMONITORS_RichMCCKPhotonPropAlg_H
#define RICHMCMONITORS_RichMCCKPhotonPropAlg_H 1

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"

// base class
#include "RichKernel/RichHistoAlgBase.h"

// MCEvent
#include "Event/MCRichOpticalPhoton.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichStatDivFunctor.h"

// tool Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RichInterfaces/IRichRefractiveIndex.h"

// boost
#include "boost/assign/list_of.hpp"

// GSL
#include "gsl/gsl_math.h"

namespace Rich
{
  namespace MC
  {

    //-----------------------------------------------------------------------------
    /** @class MCCKPhotonPropAlg RichMCCKPhotonPropAlg.h
     *
     *  Monitor algorithm to study the properties of the RICH MC CK photons
     *
     *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
     *  @date   2006-11-03
     */
    //-----------------------------------------------------------------------------

    class MCCKPhotonPropAlg final : public Rich::HistoAlgBase
    {

    public:

      /// Standard constructor
      MCCKPhotonPropAlg( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode initialize() override;    ///< Algorithm initialization
      StatusCode execute() override;    ///< Algorithm execution

    private: // data

      const IRefractiveIndex * m_refIndex = nullptr; ///< Refractive index tool

    };

  }
} // RICH namespace

#endif // RICHMCMONITORS_RichMCCKPhotonPropAlg_H
