################################################################################
# Package: HCMonitors
################################################################################
gaudi_subdir(HCMonitors v1r2)

gaudi_depends_on_subdirs(Det/DetDesc
                         Event/DAQEvent
                         Event/DigiEvent
                         Kernel/HltInterfaces
#			 Online/DIM
                         GaudiAlg
                         GaudiConf)

# hide warnings from some external projects
find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

find_package(AIDA)

gaudi_add_module(HCMonitors
                 src/*.cpp
#                 INCLUDE_DIRS AIDA Event/DigiEvent Online/DIM
#                 LINK_LIBRARIES DAQEventLib HltInterfaces DetDescLib GaudiAlgLib dim)
                 INCLUDE_DIRS AIDA Event/DigiEvent 
                 LINK_LIBRARIES DAQEventLib HltInterfaces DetDescLib GaudiAlgLib)

gaudi_install_python_modules()

