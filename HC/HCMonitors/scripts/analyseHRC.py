
from ROOT import *
from optparse import OptionParser
import xml.etree.cElementTree as ET
import xml.dom.minidom as mindom
import commands,os


import numpy as n
from array import *
from math import *
parser = OptionParser()
from subprocess import Popen, PIPE, STDOUT

parser.add_option("-a", "--analysisType", type="string",
                  help="type of analyses (NTuple, Pedestals, CommonMode)",
                  dest="analysisType")

parser.add_option("-n", "--NumberOfEvents", type="int",
                  help="Number of events to be analysed",
                  dest="NumberOfEvents" , default = -1)

parser.add_option("-r", "--runNumber", type="string",
                  help="Run number to be analysed",
                  dest="runNumber")

parser.add_option("", "--refRunNumber", type="string",
                  help="reference run number for common mode suppression",
                  dest="refRunNumber",default = "-1")

parser.add_option("-s", "--scheme", type="string",
                  help="Optimisation scheme (relativeMax, relativeMax2, absoluteMax, singleBx)",
                  dest="scheme" , default = "relativeMax")

parser.add_option("-c", "--comments", type="string",
                  help="Comments",
                  dest="comments" , default = "")

parser.add_option( '-b',"--RunBrunel",action = 'store_true',
                  help="Run Brunel (needed once at least to get the ntuple)", 
                  dest="RunBrunel",default = False)

parser.add_option("-t", "--TAE", type="int",
                  help="TAE mode, number of prev and next",
                  dest="TAE", default = -1 )

parser.add_option( "--bxIDRange", type="string",
                  help="bxID range to consider",
                  dest="bxIDRange",default = "0,10000")

parser.add_option( "--PedestalOffset", type="int",
                  help="Offset to apply to the pedestals",
                  dest="PedestalOffset" , default = 0)

parser.add_option( "--BrunelScriptName", type="string",
                  help="Name of the Brunel script", 
                  dest="BrunelScriptName",default =  'rungaudi.py')

parser.add_option( "--Directory", type="string",
                  help="Reference directory for analysed runs", 
                  dest="Directory",default = '/home/herschel/AnalysedRuns')

parser.add_option( "--DataDirectory", type="string",
                  help="Top directory for data",
                  dest="DataDirectory", default = "/daqarea/lhcb/data/2017/RAW/FULL/HRC/TEST")

parser.add_option( "--BxCentral", type="int",
                  help="",
                  dest="BxCentral" , default = 10000)
parser.add_option( "--BxNext", type="int",
                  help="",
                  dest="BxNext" , default = 10000)
parser.add_option( "--BxPrev", type="int",
                  help="",
                  dest="BxPrev" , default = 10000)
Results = {}


stations = ['B0','B1','B2','F1','F2']
for s in stations:
    parser.add_option( "--step"+s, type = 'int',
                        help="force optimal step for "+s, 
                        dest="step"+s,default = -1)


options, arguments = parser.parse_args()


### Check the abrorescence for output and if Brunel needs to be run
localDir = options.Directory+'/'+options.runNumber
availDir = os.listdir(options.Directory)
if not options.runNumber in availDir:
    print 'make directory',localDir
    os.mkdir(localDir)
    options.RunBrunel = True
    os.mkdir(localDir+'/Plots')


if options.RunBrunel:
    if options.analysisType == 'DelayScan' and options.TAE == -1: options.TAE = 1
    try:  
        os.environ["BRUNELROOT"]
    except KeyError: 
        print "No Brunel environment is setup"
        sys.exit(1)
    #version = os.environ["BRUNELROOT"].split('/')[-3].split('_')[1]
    #print version, os.environ["User_release_area"]
    scriptLocation = os.getcwd()+'/'+options.BrunelScriptName
    typeAnalysis = options.analysisType
    if options.analysisType == 'ApplyCommonMode':
        typeAnalysis = 'NTuple'
    mycommand = ['python',scriptLocation,'-a',typeAnalysis, '-r', options.runNumber, '-t' ,str(options.TAE), '-n', str(options.NumberOfEvents),'-d',options.DataDirectory ,  '--minBx',options.bxIDRange.split(',')[0],  '--maxBx',options.bxIDRange.split(',')[1],'--refRunNumber',options.refRunNumber,'-o',options.Directory]
    if options.analysisType == 'DelayScan' and options.scheme == 'singleBx' :
        mycommand.append("--BxCentral")
        mycommand.append(str(options.BxCentral))
        mycommand.append("--BxNext")
        mycommand.append(str(options.BxNext))
        mycommand.append("--BxPrev")
        mycommand.append(str(options.BxPrev))
    p = Popen(mycommand, stdout=PIPE, stdin=PIPE, stderr=STDOUT, bufsize=1)
    for line in iter(p.stdout.readline, ''):
        print line, 
    p.stdout.close()


if len(options.runNumber.split(','))>1:
   options.runNumber = options.runNumber.split(',')[0]+'_'+options.runNumber.split(',')[-1]


def fitHist(plot, name = 'gg', centered = False ):
    gau = TF1(name,"gaus",0.,500.)
    if centered:
        gau = TF1(name,"gaus",-250.,250.)
    gau.SetParameters(1,plot.GetMean())
    gau.SetParameters(2,plot.GetRMS())
    plot.Fit( gau,'QNR') 
    param = gau.GetParameters()
    gau.SetParLimits(0,param[0]*0.01,param[0]*100.)
    gau.SetParLimits(1,param[1]*0.5,param[0]*2.)
    gau.SetParLimits(2,param[2]*0.1,param[2]*10.)
    plot.Fit( gau,'QNR') 
    plot.Fit( gau,'QNR') 
    plot.Fit( gau,'QNR') 
    return gau.GetParameter(1),gau.GetParameter(2),gau

if options.analysisType == 'CommonMode':
    commonmode = ET.Element("COMMONMODE_RUN"+options.runNumber)
    xmlSide = {}
    f = TFile(options.Directory+'/'+options.runNumber+'/'+options.analysisType+'_'+options.runNumber+'.root','READ')
    Canvas = {}
    Hists,Fits = {},{}
    Results = {}
    for side in ['B','F']:
        for st in ['0','1','2']:
            if side == 'F' and st == '0': continue
            Canvas[side+st] = TCanvas(side+st,side+st)
            Canvas[side+st].Divide(4,2)
            for i,par in enumerate(['Odd','Even']):
                for j,q in enumerate(['0','1','2','3']):
                    Fits[side+st+q+par+'Plot']= TF1(side+st+q+par+"linFitPlot","[1]+[0]*x",0.,300.)
                    Fits[side+st+q+par+'Full']=TF2(side+st+q+par+"gaus2dfit","[0]*exp(-(cos([3])*cos([3])/(2*[4]*[4])+sin([3])*sin([3])/(2*[5]*[5]))*(x-[1])*(x-[1])-2*(-sin(2*[3])/(4*[4]*[4])+sin(2*[3])/(4*[5]*[5]))*(x-[1])*(y-[2])-(sin([3])*sin([3])/(2*[4]*[4])+cos([3])*cos([3])/(2*[5]*[5]))*(y-[2])*(y-[2]))")
                    Hists[side+st+q+par+'2D'] = f.Get('HCPedestalCorrection/Correlation/'+side+st+'/'+q+'/'+par)
                    Canvas[side+st].cd(j+4*i+1)
                    rmsX,rmsY = Hists[side+st+q+par+'2D'].GetRMS(1),Hists[side+st+q+par+'2D'].GetRMS(2)
                    if rmsX == 0.: 
                        print "RMS X null,",side+st+q+par
                        rmsX = rmsY
                    theta = TMath.ATan(rmsY/rmsX)
                    N = Hists[side+st+q+par+'2D'].GetEntries()
                    Fits[side+st+q+par+'Full'].SetParameter(0,N)
                    meanX,meanY = Hists[side+st+q+par+'2D'].GetMean(1),Hists[side+st+q+par+'2D'].GetMean(2)
                    Fits[side+st+q+par+'Full'].SetParameter(1,meanX)	#x_0
                    Fits[side+st+q+par+'Full'].SetParameter(2,meanY)	#y_0
                    Fits[side+st+q+par+'Full'].SetParameter(3,theta)	#theta  (estimated value from linear fit)
                    Fits[side+st+q+par+'Full'].SetParLimits(3,0.5,1.5)#theta limits
                    Fits[side+st+q+par+'Full'].SetParameter(4,5)	#sigma_x
                    Fits[side+st+q+par+'Full'].SetParameter(5,20)	#sigma_y
                    Fits[side+st+q+par+'Full'].SetParLimits(4,1.,20.)
                    Fits[side+st+q+par+'Full'].SetParLimits(5,10.,80.)
                    Fits[side+st+q+par+'Full'].SetContour(5)
                    Fits[side+st+q+par+'Full'].SetNpy(300)
                    Fits[side+st+q+par+'Full'].SetNpx(300)
                    Fits[side+st+q+par+'FullResult'] = Hists[side+st+q+par+'2D'].Fit(side+st+q+par+'gaus2dfit','QS')
                    theta=Fits[side+st+q+par+'Full'].GetParameter(3)
                    x0=Fits[side+st+q+par+'Full'].GetParameter(1)
                    y0=Fits[side+st+q+par+'Full'].GetParameter(2)
                    rmsX=Fits[side+st+q+par+'Full'].GetParameter(4)
                    rmsY=Fits[side+st+q+par+'Full'].GetParameter(5)
                    a_New =TMath.Tan(TMath.Pi()/2 - theta)
                    b_New = y0 - (a_New*x0)
                    xmlSide[side+st+q+par] = ET.SubElement(commonmode,side+st+q+par )
                    ET.SubElement(xmlSide[side+st+q+par], "theta", name="correlation").text = str(theta)
                    ET.SubElement(xmlSide[side+st+q+par], "X0", name="Mean Reference").text = str(x0)
                    ET.SubElement(xmlSide[side+st+q+par], "Y0", name="Mean Signal").text = str(y0)
                    ET.SubElement(xmlSide[side+st+q+par], "RMSUncorrected", name="Uncorrected RMS").text = str(rmsY)
                    Results[side+st+q+par]=[theta,x0,y0]
                    Fits[side+st+q+par+'Plot'].SetParameters(a_New,b_New)
                    Fits[side+st+q+par+'Plot'].SetLineColor(4)
                    Fits[side+st+q+par+'Full'].SetLineColor(4)
                    Hists[side+st+q+par+'2D'].Draw('col4z')
                    Fits[side+st+q+par+'Plot'].SetRange(0.,300.)
                    Fits[side+st+q+par+'Plot'].Draw("same")
            
            Canvas[side+st].SaveAs(options.Directory+'/'+options.runNumber+'/Plots/'+options.analysisType+'_'+options.runNumber+'_'+side+st+'.pdf')

    tree =  ET.ElementTree(commonmode)
    tree.write(options.Directory+'/'+options.runNumber+'/'+options.analysisType+'_'+options.runNumber+".xml")

elif options.analysisType == 'Pedestals':
    ftex = open(options.Directory+'/'+options.runNumber+'/Pedestals_'+options.runNumber+'.tex','w')
    ftex.write(
    """
    \\documentclass{beamer}
    \\usepackage{graphicx}
    \\begin{document}
    \\title{Pedestal analysis of run """+options.runNumber+""" }
    \\subtitle{"""+options.comments+"""}   
    \\author{Herschel} 
    \\date{\\today} 

    \\frame{\\titlepage} 
    """
    )
    stations = ['B0','B1','B2','F1','F2']
    for s in stations:
        Results[s]={'VFEclock':-1,'ADCclock':-1,'OddPedestals':{},'EvenPedestals':{},'OddPedestalsFit':{},'EvenPedestalsFit':{},'OddPedestalsRMS':{},'EvenPedestalsRMS':{},
                    'OddPedestalsCor':{},'EvenPedestalsCor':{},'OddPedestalsFitCor':{},'EvenPedestalsFitCor':{},'OddPedestalsRMSCor':{},'EvenPedestalsRMSCor':{}}

    commonmode = ET.Element("PEDESTAL_RUN"+options.runNumber)
    xmlSide = {}

    f = TFile(options.Directory+'/'+options.runNumber+'/'+options.analysisType+'_'+options.runNumber+'.root','READ')
    gStyle.SetOptStat("")
    slots = ['Odd','Even']
    quadrants = ['0','1','2','3']
    slotColor = {'Odd':kBlue,'Even':kRed}
    Plots = {}
    Canvas = {}
    for s in stations:
        Canvas[s+'Pedestals']=TCanvas(s+'Pedestals',s+'Pedestals',1200,1000)
        Canvas[s+'Pedestals'].Divide(2,2)
        if int(options.refRunNumber) > 0:
          Canvas[s+'PedestalsCor']=TCanvas(s+'PedestalsCor',s+'PedestalsCor',1200,1000)
          Canvas[s+'PedestalsCor'].Divide(2,2)
        for ii,q in enumerate(quadrants):
            opt = ''
            ymax = 0.
            ymaxCor = 0.
            for sl in slots:
              Plots[s+q+sl+'_pedestals']=f.Get('HCDigitMonitor/ADC/'+s+'/'+sl+'/Quadrant'+q)
              if Plots[s + q + sl + "_pedestals"].GetMaximum() > ymax:
                ymax = Plots[s + q + sl + "_pedestals"].GetMaximum()
              if int(options.refRunNumber) > 0:
                Plots[s+q+sl+'_pedestals_Cor']=f.Get('HCCorrectedDigitMonitor/ADC/'+s+'/'+sl+'/Quadrant'+q)
                if Plots[s + q + sl + "_pedestals_Cor"].GetMaximum() > ymaxCor:
                  ymaxCor = Plots[s + q + sl + "_pedestals_Cor"].GetMaximum()
            for sl in slots:
                Plots[s+q+sl+'_pedestals'].GetXaxis().SetTitle(s+q+' ADC')
                Plots[s+q+sl+'_pedestals'].SetLineColor(slotColor[sl])
                Plots[s+q+sl+'_pedestals'].SetMarkerColor(slotColor[sl])
                #Plots[s+q+sl+'_pedestals'].GetXaxis().SetRangeUser(0,200)
                Plots[s + q + sl + "_pedestals"].GetYaxis().SetRangeUser(0., ymax)
                mean,rms,Results[s][sl+'PedestalsFit'][q] = fitHist(Plots[s+q+sl+'_pedestals'],s+q+sl+'_pedestalsFit')
                Results[s][sl+'Pedestals'][q] = int(mean)
                Results[s][sl+'PedestalsRMS'][q] = round(rms,1)
                xmlSide[s+q+sl] = ET.SubElement(commonmode,s+q+sl )
                ET.SubElement(xmlSide[s+q+sl], "mean", name="Pedestal Mean Value").text = str(round(mean,2))
                ET.SubElement(xmlSide[s+q+sl], "rms", name="Pedestal RMS Value").text = str(round(rms,2))
                if int(options.refRunNumber) > 0:
                  Plots[s+q+sl+'_pedestals_Cor'].GetXaxis().SetTitle(s+q+' ADC')
                  Plots[s+q+sl+'_pedestals_Cor'].SetLineColor(slotColor[sl])
                  Plots[s+q+sl+'_pedestals_Cor'].SetMarkerColor(slotColor[sl])
                  Plots[s + q + sl + "_pedestals_Cor"].GetYaxis().SetRangeUser(0., ymaxCor)
                  meanCor,rmsCor,Results[s][sl+'PedestalsFitCor'][q] = fitHist(Plots[s+q+sl+'_pedestals_Cor'],s+q+sl+'_pedestalsFitCor',True)
                  xmlSide[s+q+sl+'Cor'] = ET.SubElement(commonmode,s+q+sl+'Cor' )
                  ET.SubElement(xmlSide[s+q+sl], "mean_Cor", name="Corrected Pedestal Mean Value").text = str(round(meanCor,2))
                  ET.SubElement(xmlSide[s+q+sl], "rms_Cor", name="Corrected Pedestal RMS Value").text = str(round(rmsCor,2))

                Canvas[s+'Pedestals'].cd(ii+1)
                Results[s][sl+'PedestalsFit'][q].SetLineColor(slotColor[sl])
                Plots[s+q+sl+'_pedestals'].Draw(opt)
                opt = 'same'
                Results[s][sl+'PedestalsFit'][q].Draw(opt)
                if int(options.refRunNumber) > 0:
                  Canvas[s+'PedestalsCor'].cd(ii+1)
                  Results[s][sl+'PedestalsFitCor'][q].SetLineColor(slotColor[sl])
                  Plots[s+q+sl+'_pedestals_Cor'].Draw(opt)
                  opt = 'same'
                  Results[s][sl+'PedestalsFitCor'][q].Draw(opt)
                #Canvas[s+'Pedestals'].GetPad(ii+1).SetLogy()
                
        Canvas[s+'Pedestals'].SaveAs(options.Directory+'/'+options.runNumber+'/Plots/'+options.analysisType+'_'+options.runNumber+'_'+s+'_Pedestals.pdf')
        if int(options.refRunNumber) > 0:
          Canvas[s+'PedestalsCor'].SaveAs(options.Directory+'/'+options.runNumber+'/Plots/'+options.analysisType+'_'+options.runNumber+'_'+s+'_PedestalsCor.pdf')
    
        ftex.write("""   
        \\frame{
        \\frametitle{"""+s+""" Pedestals} 
        \\framesubtitle{ADCs}
        \\begin{center}
        \\includegraphics[height=8cm]{"""+options.Directory+'/'+options.runNumber+'/Plots/'+options.analysisType+'_'+options.runNumber+'_'+s+'_Pedestals.pdf'+"""}
        \\end{center}
        }
        """)   

        ftex.write("""   
        \\frame{
        \\frametitle{"""+s+""" Pedestals} 
        \\framesubtitle{Mean and RMS}
        \\begin{itemize}
        \\item Pedestals:
        \\begin{itemize}
        \\item Quadrant 0: Odd """ +str(Results[s]['OddPedestals']['0'])+'('+str(Results[s]['OddPedestalsRMS']['0'])+') Even '+str(Results[s]['EvenPedestals']['0'])+'('+str(Results[s]['EvenPedestalsRMS']['0'])+')'+"""
        \\item Quadrant 1: Odd """ +str(Results[s]['OddPedestals']['1'])+'('+str(Results[s]['OddPedestalsRMS']['1'])+') Even '+str(Results[s]['EvenPedestals']['1'])+'('+str(Results[s]['EvenPedestalsRMS']['1'])+')'+"""
        \\item Quadrant 2: Odd """ +str(Results[s]['OddPedestals']['2'])+'('+str(Results[s]['OddPedestalsRMS']['2'])+') Even '+str(Results[s]['EvenPedestals']['2'])+'('+str(Results[s]['EvenPedestalsRMS']['2'])+')'+"""
        \\item Quadrant 3: Odd """ +str(Results[s]['OddPedestals']['3'])+'('+str(Results[s]['OddPedestalsRMS']['3'])+') Even '+str(Results[s]['EvenPedestals']['3'])+'('+str(Results[s]['EvenPedestalsRMS']['3'])+')'+"""
        \\end{itemize}
        \\end{itemize}
        }
        """)

    ftex.write("""   
        \\frame{
        \\frametitle{Pedestals} 
        \\framesubtitle{BW Configuration for offset of """+str(options.PedestalOffset)+"""}
        \\begin{tiny}
        """)
    ftex.write('FEPGA0\_o255sets 255 255 255 255 255 255 '+str(Results['B0']['EvenPedestals']['0']-options.PedestalOffset)+' '+str(Results['B0']['OddPedestals']['0']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B0']['EvenPedestals']['1']-options.PedestalOffset)+' '+str(Results['B0']['OddPedestals']['1']-options.PedestalOffset)+'\\\\')
    ftex.write('FEPGA1\_o255sets 255 255 255 255 255 255 '+str(Results['B0']['EvenPedestals']['2']-options.PedestalOffset)+' '+str(Results['B0']['OddPedestals']['2']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B0']['EvenPedestals']['3']-options.PedestalOffset)+' '+str(Results['B0']['OddPedestals']['3']-options.PedestalOffset)+'\\\\')
    ftex.write('FEPGA2\_o255sets 255 255 255 255 '+str(Results['B2']['EvenPedestals']['0']-options.PedestalOffset)+' '+str(Results['B2']['OddPedestals']['0']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B2']['EvenPedestals']['1']-options.PedestalOffset)+' '+str(Results['B2']['OddPedestals']['1']-options.PedestalOffset)+' 255 255'+'\\\\')
    ftex.write('FEPGA3\_o255sets 255 255 255 255 '+str(Results['B2']['EvenPedestals']['2']-options.PedestalOffset)+' '+str(Results['B2']['OddPedestals']['2']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B2']['EvenPedestals']['3']-options.PedestalOffset)+' '+str(Results['B2']['OddPedestals']['3']-options.PedestalOffset)+' 255 255'+'\\\\')
    ftex.write('FEPGA4\_o255sets 255 255 '+str(Results['B1']['EvenPedestals']['0']-options.PedestalOffset)+' '+str(Results['B1']['OddPedestals']['0']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B1']['EvenPedestals']['1']-options.PedestalOffset)+' '+str(Results['B1']['OddPedestals']['1']-options.PedestalOffset)+' 255 255 255 255'+'\\\\')
    ftex.write('FEPGA5\_o255sets 255 255 '+str(Results['B1']['EvenPedestals']['2']-options.PedestalOffset)+' '+str(Results['B1']['OddPedestals']['2']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['B1']['EvenPedestals']['3']-options.PedestalOffset)+' '+str(Results['B1']['OddPedestals']['3']-options.PedestalOffset)+' 255 255 255 255'+'\\\\')
    ftex.write('FEPGA6\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')
    ftex.write('FEPGA7\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')
    ftex.write("""   
        \\end{tiny}
        }
        \\frame{
        \\frametitle{Pedestals} 
        \\framesubtitle{FW Configuration for offset of """+str(options.PedestalOffset)+"""}
        \\begin{tiny}
        """)
    ftex.write('FEPGA0\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')
    ftex.write('FEPGA1\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')
    ftex.write('FEPGA2\_o255sets 255 255 255 255 '+str(Results['F2']['OddPedestals']['0']-options.PedestalOffset)+' '+str(Results['F2']['EvenPedestals']['0']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['F2']['OddPedestals']['1']-options.PedestalOffset)+' '+str(Results['F2']['EvenPedestals']['1']-options.PedestalOffset)+' 255 255 '+'\\\\')
    ftex.write('FEPGA3\_o255sets 255 255 255 255 '+str(Results['F2']['OddPedestals']['2']-options.PedestalOffset)+' '+str(Results['F2']['EvenPedestals']['2']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['F2']['OddPedestals']['3']-options.PedestalOffset)+' '+str(Results['F2']['EvenPedestals']['3']-options.PedestalOffset)+' 255 255 '+'\\\\')

    ftex.write('FEPGA4\_o255sets 255 255 '+str(Results['F1']['OddPedestals']['0']-options.PedestalOffset)+' '+str(Results['F1']['EvenPedestals']['0']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['F1']['OddPedestals']['1']-options.PedestalOffset)+' '+str(Results['F1']['EvenPedestals']['1']-options.PedestalOffset)+' 255 255 255 255'+'\\\\')
    ftex.write('FEPGA5\_o255sets 255 255 '+str(Results['F1']['OddPedestals']['2']-options.PedestalOffset)+' '+str(Results['F1']['EvenPedestals']['2']-options.PedestalOffset)+' 255 255 255 255 255 255 '+str(Results['F1']['OddPedestals']['3']-options.PedestalOffset)+' '+str(Results['F1']['EvenPedestals']['3']-options.PedestalOffset)+' 255 255 255 255'+'\\\\')
    ftex.write('FEPGA6\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')
    ftex.write('FEPGA7\_o255sets 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255'+'\\\\')

    ftex.write(""" 
        \\end{tiny}
        }
        
    \end{document}
    """)
    ftex.close()

    commands.getoutput('pdflatex '+options.Directory+'/'+options.runNumber+'/Pedestals_'+options.runNumber+'.tex')
    commands.getoutput('mv Pedestals_'+options.runNumber+'.pdf '+options.Directory+'/'+options.runNumber+'/Pedestals_'+options.runNumber+'.pdf')
    commands.getoutput('rm Pedestals_'+options.runNumber+'.*')

    tree =  ET.ElementTree(commonmode)
    tree.write(options.Directory+'/'+options.runNumber+'/'+options.analysisType+'_'+options.runNumber+".xml")

elif options.analysisType == 'PMTIntensity':
    Channel,Pedestal,sigma,mean,gau = {},{},{},{},{}
    slope,exp,stepFcn,truncExp,Channel,hrcSignal,hrc0Signal,frac0Signal,hrc = {},{},{},{},{},{},{},{},{}
    myset = RooArgSet()
    for v in ['B00','B01','B02','B03','B10','B11','B12','B13','B20','B21','B22','B23','F10','F11','F12','F13','F20','F21','F22','F23']:
        Channel[v] = RooRealVar(v,v,-100.,800.)
        Pedestal[v] = RooRealVar('pedestal'+v,'pedestal'+v,0.,-200.,200.)
        sigma[v] = RooRealVar('sigma'+v,'sigma'+v,20.,5.,50.)
        mean[v] = RooRealVar('mean'+v,'mean'+v,0.)
        gau[v] = RooGaussian('gaus'+v,'gaus'+v,Channel[v],mean[v],sigma[v])
        slope[v] = RooRealVar('slope'+v,'slope'+v,-6.25300e-03,-1.e-1,-1.e-4)
        exp[v] = RooExponential("model"+v,"model"+v,Channel[v],slope[v])
        stepFcn[v] = RooFormulaVar('step'+v,v+' > pedestal'+v+' ? 1. : 0.',RooArgList(Channel[v],Pedestal[v]))
        truncExp[v] = RooEffProd ("truncExp"+v,"truncExp"+v,exp[v],stepFcn[v])
        Channel[v].setBins(10000,"cache")
        hrcSignal[v] = RooFFTConvPdf("hrcSignal"+v,"hrcSignal"+v,Channel[v],truncExp[v],gau[v])
        hrc0Signal[v] = RooGaussian('hrc0Signal'+v,'hrc0Signal+v',Channel[v],Pedestal[v],sigma[v])
        frac0Signal[v]= RooRealVar("frac0Signal"+v,"fraction of empty events"+v,0.2,0.,1.)
        hrc[v] =  RooAddPdf("hrc","hrc",hrc0Signal[v],hrcSignal[v],frac0Signal[v])
        myset.add(Channel[v])
    
    Canvas,frame={},{}
    myChain = TChain('Tree')
    myChain.Add('ApplyCommonMode_'+options.runNumber+'.root')
    dataset = RooDataSet("dataset","",myset,RooFit.Import(myChain))
    for side in ['B','F']:
        for station in ['0','1','2']:
            if side == 'F' and station == '0': continue
            Canvas[side+station]= TCanvas(side+station,side+station)
            Canvas[side+station].Divide(2,2)
            for kk, quad in enumerate(['0','1','2','3']):
                Canvas[side+station].cd(kk+1)
                hrc[side+station+quad].fitTo(dataset,RooFit.Minos(True),RooFit.Strategy(0),RooFit.Save(True),RooFit.NumCPU(2),RooFit.Verbose(False))
                frame[side+station+quad] = Channel[side+station+quad].frame()
                dataset.plotOn(frame[side+station+quad])
                hrc[side+station+quad].plotOn(frame[side+station+quad])
                frame[side+station+quad].Draw()
                
                
