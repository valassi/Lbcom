from ROOT import TCanvas, TGraph, TH1D, TH2D, gROOT, TFile, TF1, TRandom3, TMath
from ROOT import kRed, kBlue, kGreen
import copy
gROOT.SetBatch()

## Get the dataset
import sys
if len(sys.argv) !=2 :
    print "Need runNumber"
    sys.exit()
runnumber = sys.argv[1]
stationsAll = ['B0','B1','B2','F1','F2']

def ROC(h1,h2):
    g = TGraph()
    g.SetName(h1.GetName()+'ROC')
    g.SetTitle(h1.GetName()+'ROC')
    for i in range(h1.GetNbinsX()-4):
        if  h1.GetBinContent(i) == 1. : continue
        if  h2.GetBinContent(i) == 0. : continue
        #print h1.GetBinContent(i),1.-h2.GetBinContent(i)
        g.SetPoint(i,h1.GetBinContent(i),1-h2.GetBinContent(i))
    return g

def convolution(h1,h2):
    A1 = h1.GetArray()
    A2 = h2.GetArray()
    N1 = h1.GetEntries()
    N2 = h2.GetEntries()
    newArray = [0.]*h2.GetNbinsX()*2
    A2mean = h2.GetXaxis().FindBin(h2.GetMean())
    for i in range(h1.GetNbinsX()):
        a1 = A1[i]
        if a1 == 0. : continue
        for j in range(h2.GetNbinsX()):
            a2 = A2[j]
            if a2 == 0.: continue
            if i+j-A2mean<0.: continue
            newArray[i+j-A2mean]+=a1*a2/N2

    hNew = h1.Clone('Sum'+h1.GetName())
    hNew.Reset()
    for i in range(hNew.GetNbinsX()):
        if hNew.GetXaxis().GetBinCenter(i)>1000.:
            hNew.SetBinContent(hNew.GetXaxis().FindBin(1000.),hNew.GetBinContent(hNew.GetXaxis().FindBin(1000.))+newArray[i]) 
        else:
            hNew.SetBinContent(i,newArray[i]) 
    
    n = 0.
    for i in range(hNew.GetNbinsX()+1):
        n+=hNew.GetBinContent(i)
    for i in range(hNew.GetNbinsX()+1):
        if n>0 :
            hNew.SetBinContent(i,hNew.GetBinContent(i)/n)
    
    return hNew



def signalFunction(sig,pedestal,rand,N):
    expo = TF1('expon','expo',500.,900.)
    h = sig.Fit(expo,"Q","",500.,900.)
    hNew = sig.Clone('Expo'+sig.GetName())
    hNew.Reset()
    binshift = pedestal.GetXaxis().FindBin(pedestal.GetMean())
    slope = -1*expo.GetParameter(1)
    for ix in range(binshift,hNew.GetNbinsX()):
        x = hNew.GetXaxis().GetBinCenter(ix)
        hNew.SetBinContent(ix,TMath.Exp(-slope * x))
    '''
    i = 0 
    while i<N:
        k = rand.Exp(-1./expo.GetParameter(1))*1000
        k = TMath.Exp(-k/expo.GetParameter(1))
        if k>900.: hNew.Fill(901.)
        else: hNew.Fill(k)
        i+=1
    '''
    return [ convolution(hNew,pedestal),slope ]
        
def VFEandADC(step):
    return step % 32 , int( step / 32 ) 


########################################
### Create outputFile
########################################
f = TFile('/calib/hrc/AnalysedRuns/'+runnumber+'/DelayScan_'+runnumber+'.root','r')
f_out = TFile('/calib/hrc/AnalysedRuns/'+runnumber+'/ROC_'+runnumber+'.root','recreate')

########################################
# Create the empty histograms, all for 
# the three L0 slots central/prev/next
# histograms for the basic distribution adn
# for sum (convolution of prev and next,
# cumul, mc, roc etc
# for each qudrant in reference (ee) and normal (bb)
# for each step 0->1024
########################################
rand = TRandom3()
maxStep = 1024
slots = ['Central','Prev','Next']
slotNames = {'Central':'Central' , 'Next':'Next', 'Prev':'Previous' }
### Create the histograms for scan analysis
### Also, store the activity slope
hist = {}
slope = {}
bxIDList = ["head","tail"]
for bxid in bxIDList:
    slope[bxid] = {}
    for station in stationsAll:
        slope[bxid][station] = {}
        for q in ['0','1','2','3']:
            slope[bxid][station][q] = {}
print 'Getting Histograms'
for bxid in bxIDList:
    hist[bxid] = {}
    print bxid
    for slot in slots+['Sum','Cumul','MC','MCCumul','ROC']:   
        hist[bxid][slot] = {}
        hist[bxid][slot+'Ref'] = {}
        print slot
        print stationsAll
        for station in stationsAll:
            hist[bxid][slot][station] = {}
            hist[bxid][slot+'Ref'][station] = {}
            for q in ['0','1','2','3']:
                if slot not in ['Central','Prev','Next']:
                    print "  "+station+q
                hist[bxid][slot][station][q] = {}
                hist[bxid][slot+'Ref'][station][q] = {}
                for step in xrange(maxStep):
                    if slot == 'Sum':
                        print 'In Sum, trying for ',bxid,slot,station,q,step
                        print hist[bxid]['Prev'][station][q][step]
                        print hist[bxid]['Next'][station][q][step]
                        hist[bxid][slot][station][q][step]=convolution(hist[bxid]['Prev'][station][q][step],hist[bxid]['Next'][station][q][step])
                        hist[bxid][slot+'Ref'][station][q][step]=convolution(hist[bxid]['Prev'+'Ref'][station][q][step],hist[bxid]['Next'+'Ref'][station][q][step])
                        #hist[bxid][slot][station][q][step] = hist[bxid]['Prev'][station][q][step].Clone(slot+str(step)+station+q+'_'+bxid)
                        #hist[bxid][slot+'Ref'][station][q][step] = hist[bxid]['PrevRef'][station][q][step].Clone(slot+'Ref'+str(step)+station+q+'_'+bxid)
                        #hist[bxid][slot][station][q][step].Add(hist[bxid]['Next'][station][q][step].Clone(slot+str(step)+station+q+'_'+bxid))
                        #hist[bxid][slot+'Ref'][station][q][step].Add(hist[bxid]['NextRef'][station][q][step].Clone(slot+str(step)+station+q+'_'+bxid))
                    elif slot == 'ROC':
                        hist[bxid][slot][station][q][step]=ROC(hist[bxid]['Cumul'][station][q][step],hist[bxid]['MCCumul'][station][q][step])
                        print 'At the end of ROC stationsAll is :'
                        print stationsAll
                    elif slot == 'Cumul':
                        hist[bxid][slot][station][q][step]= hist[bxid]['Sum'][station][q][step].GetCumulative()
                        #for i in range(hist[bxid][slot][station][q][step].GetNbinsX()):
                        #    hist[bxid][slot][station][q][step].SetBinContent(i,1-hist[bxid][slot][station][q][step].GetBinContent(i))
                    elif slot == 'MC':
                        myList = signalFunction(hist[bxid]['Central'][station][q][step],hist[bxid]['Sum'][station][q][step],rand,10000)
                        hist[bxid][slot][station][q][step]= myList[0]
                        slope[bxid][station][q][step]= myList[1]
                        f_out.cd()
                        hist[bxid][slot][station][q][step].Write("hist_"+str(bxid)+slot+station+q+str(step))
                    elif slot == 'MCCumul':
                        hist[bxid][slot][station][q][step]=hist[bxid]['MC'][station][q][step].GetCumulative()
                    else:
                        hist[bxid][slot][station][q][step]       = f.Get( 'HCDelayScan/ADC/'+str(step)+'/'+station+'/'+q+'/'+slotNames[slot]+'/bb_'+bxid)
                        hist[bxid][slot+'Ref'][station][q][step] = f.Get( 'HCDelayScan/ADC/'+str(step)+'/'+station+'/'+q+'/'+slotNames[slot]+'/ee_'+bxid)
print "At the end of the loop, stationsAll is:"
print stationsAll

########################################
# Prepare a chi2 histogram in 2D that will
# in fact store inverse of chi2 to allow easier
# visualisation
# "chi2" quantifies difference between reference
# and signal channel ie should be minimised in the 
# prev and next slots (which, as it happens, automatically
# means that signal is well confined in the central slot)
# Also prepare an "average activity" histogram in 2D
# that allows one to view the average activity in 
# the central trigger window 
print 'Preparing chi2 and slope histograms'
histChi2 = {}
histChi2Total ={}
histChi22d = {}
histChi2Total2D ={}
histMean = {}
histMean2d = {}
stationsListHere = copy.deepcopy(stationsAll)
if 'B0' in stationsAll and 'B2' in stationsAll :
    stationsListHere.append('B0andB2')
for station in stationsListHere :
    histChi2[station] = {}
    histChi2Total[station] = TH1D('chi2Total'+station,'chi2Total'+station,maxStep,0.,maxStep)
    histChi2Total2D[station] = TH2D('chi2Total2D'+station,'chi2Total2D'+station,32,0.,32.,32,0.,32.)
    histMean[station] = {}
    histMean2d[station] = {}
    histChi22d[station] = {}
    for q in ['0','1','2','3']:
        histChi2[station][q] = TH1D('chi2'+station+q,'chi2'+station+q,maxStep,0.,maxStep)
        histChi22d[station][q] = TH2D('chi22D'+station+q,'chi22D'+station+q,32,0.,32.,32,0.,32.)
        histMean[station][q] = TH1D('mean'+station+q,'mean'+station+q,maxStep,0.,maxStep)
        histMean2d[station][q] = TH2D('mean2D'+station+q,'mean2D'+station+q,32,0.,32.,32,0.,32.)
    


# Now compute the chi2
bestValue = {'B0':-1.,'B1':-1.,'B2':-1.,'F1':-1.,'F2':-1.,'B0andB2':-1.}
bestRMS = {'B0':[10.]*4,'B1':[10.]*4,'B2':[10.]*4,'F1':[10.]*4,'F2':[10.]*4}
bestStep = {'B0':-1,'B1':-1,'B2':-1,'F1':-1,'F2':-1}
for step in xrange(maxStep):
    vfe,adc = VFEandADC(step)
    tmp_chi2 = {}
    for station in stationsAll :
        tmp_chi2[station] = 0
        chi2Total = 0
        for q in ['0','1','2','3']:
            # if average of distribution in prev or next is greater than central then assume this delay setting means that we are looking at the wrong bxid and give it 'zero' 
            # in the chi2 histogram
            if hist[bxIDList[0]]['Central'][station][q][step].GetMean()<hist[bxIDList[0]]['Prev'][station][q][step].GetMean() or hist[bxIDList[0]]['Central'][station][q][step].GetMean()<hist[bxIDList[0]]['Next'][station][q][step].GetMean():
                continue
            # Now plot average activity for this bxID
            print 'Trying',bxid,station,q,step
            if slope[bxid][station][q][step] > 0 :
                histMean2d[station][q].Fill( vfe,adc, 1./slope[bxid][station][q][step] )
            chi2 = 0.
            if hist[bxid]['PrevRef'][station][q][step].GetRMS()  != 0. and \
                hist[bxid]['PrevRef'][station][q][step].GetMean() != 0. and \
                hist[bxid]['NextRef'][station][q][step].GetRMS() != 0. and \
                hist[bxid]['NextRef'][station][q][step].GetMean() != 0. :
                chi2 +=  (hist['head']['Prev'][station][q][step].GetRMS()-hist['head']['PrevRef'][station][q][step].GetRMS())**2/hist['head']['PrevRef'][station][q][step].GetRMS()
                chi2 +=  (hist['tail']['Next'][station][q][step].GetRMS()-hist['tail']['NextRef'][station][q][step].GetRMS())**2/hist['tail']['NextRef'][station][q][step].GetRMS()  
            chi2Total += chi2
            tmp_chi2[station] = chi2Total
            # Now actually fill the inverse in the "histChi2" distribution to allow easier visualisation of maximum
            if chi2>0.:
                histChi2[station][q].SetBinContent(histChi2[station][q].GetXaxis().FindBin(step),1000./chi2)
                histChi22d[station][q].Fill( vfe, adc, 1000./chi2 )
        if chi2Total>0.:
            if 1000./chi2Total>bestValue[station]:
                bestStep[station] = step
                bestValue[station] = 1000./chi2Total
                for i, q in enumerate(['0','1','2','3']):
                    bestRMS[station][i] = 0.
                    for bxid in bxIDList:
                        bestRMS[station][i]+= hist[bxid]['Sum'][station][q][step].GetRMS()/len(bxIDList)
            histChi2Total[station].SetBinContent(histChi2[station][q].GetXaxis().FindBin(step),1000./chi2Total)
            histChi2Total2D[station].Fill(vfe,adc,1000./chi2Total)
        if station == "B2" :
            if tmp_chi2["B0"]>0. and tmp_chi2["B2"]>0. :
                if 1000./(tmp_chi2["B0"]+tmp_chi2["B2"]) > bestValue["B0andB2"]:
                    bestStep["B0andB2"] = step
                    bestValue["B0andB2"] = 1000./(tmp_chi2["B0"]+tmp_chi2["B2"])
                histChi2Total["B0andB2"].Fill( histChi2["B0andB2"][q].GetXaxis().FindBin(step),1000./(tmp_chi2["B0"]+tmp_chi2["B2"]) )
                histChi2Total2D["B0andB2"].Fill( vfe, adc, 1000./(tmp_chi2["B0"]+tmp_chi2["B2"]) )


c = {}
for station in stationsAll :
    print 'Best step for station',station,VFEandADC(bestStep[station]),'Chi2',bestValue[station]
    c[station] = TCanvas(station+sys.argv[1],station+sys.argv[1])
    c[station].Divide(2,2)
    for i,q in enumerate(['0','1','2','3']):
        c[station].cd(i+1)
        histChi2[station][q].Draw()
        histChi2Total[station].SetLineColor(kRed)
        histChi2Total[station].Draw('same')
        histMean[station][q].SetLineColor(kBlue)
        histMean[station][q].Draw('same')
        f_out.cd()
        histChi2[station][q].Write("histChi2_"+station+str(q))
        histChi2Total[station].Write("histChi2_"+station)
        histMean[station][q].Write("histMean_"+station+str(q))
        histChi22d[station][q].Write("histChi22d_"+station+str(q))

# Now find the best point as a function of ADC for B0 and B2.
if 'B0' in stationsAll and 'B2' in stationsAll :
    print '--> Now doing combined optimisation for B0 and B2'
    print '    Best simple step for "B0andB2"',"B0andB2",VFEandADC(bestStep["B0andB2"]),'Chi2',bestValue["B0andB2"]
    print '    Now vary ADC for each to try and improve'
    numVFEbin = histChi2Total2D["B0andB2"].GetXaxis().FindBin( VFEandADC(bestStep["B0andB2"])[0] )
    h_b0_bestVFE = histChi2Total2D["B0"].ProjectionY("b0_bestVFE",numVFEbin,numVFEbin)
    print '    Best B0 point is (',VFEandADC(bestStep["B0andB2"])[0],',',(2*(-1+h_b0_bestVFE.GetMaximumBin())),')'
    h_b2_bestVFE = histChi2Total2D["B2"].ProjectionY("b2_bestVFE",numVFEbin,numVFEbin)
    print '    Best B2 point is (',VFEandADC(bestStep["B0andB2"])[0],',',(2*(-1+h_b2_bestVFE.GetMaximumBin())),')'

for station in stationsAll :
    print 'Best step for station',station,VFEandADC(bestStep[station])
    c[station+'hist'] = TCanvas(station+'optimal'+'hist'+sys.argv[1],station+'optimal'+'hist'+sys.argv[1])
    c[station+'hist'].Divide(2,2)
    c[station+'histmean2d'] = TCanvas(station+'optimal'+'histmean2d'+sys.argv[1],station+'optimal'+'histmean2d'+sys.argv[1])
    c[station+'histmean2d'].Divide(2,2)
    c[station+'histchi22d'] = TCanvas(station+'optimal'+'histchi22d'+sys.argv[1],station+'optimal'+'histchi22d'+sys.argv[1])
    c[station+'histchi22d'].Divide(2,2)
    c[station+'2D'] = TCanvas(station+'optimal'+'2D'+sys.argv[1],station+'optimal'+'2D'+sys.argv[1])
    histChi2Total2D[station].Draw('col4z')
    c[station+"2D"].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/2Dchi2Total_"+station+".pdf")
    if station=="B0" :
        histChi2Total2D["B0andB2"].Draw('col4z')
        c[station+"2D"].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/2Dchi2Total_B0andB2.pdf")
    f_out.cd()
    histChi2Total2D[station].Write("histChi2Total2D_"+station)
    for i,q in enumerate(['0','1','2','3']):
        c[station+'hist'].cd(i+1)
        c[station+'hist'].cd(i+1).SetLogy()
        hist[bxIDList[0]]['Central'][station][q][bestStep[station]].SetLineColor(kGreen)
        hist[bxIDList[0]]['Central'][station][q][bestStep[station]].Draw()
        hist[bxIDList[0]]['Sum'][station][q][bestStep[station]].SetLineColor(kRed)
        hist[bxIDList[0]]['Sum'][station][q][bestStep[station]].Draw('same')
        hist[bxIDList[0]]['Next'][station][q][bestStep[station]].SetLineColor(kBlue-4)
        hist[bxIDList[0]]['Next'][station][q][bestStep[station]].Draw('same')
        hist[bxIDList[0]]['Prev'][station][q][bestStep[station]].SetLineColor(kBlue+4)
        hist[bxIDList[0]]['Prev'][station][q][bestStep[station]].Draw('same')
        c[station+'histmean2d'].cd(i+1)
        histMean2d[station][q].Draw('col4z')
        c[station+'histchi22d'].cd(i+1)
        histChi22d[station][q].Draw('col4z')
    c[station+'hist'].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/hist_"+station+".pdf")
    c[station+'histmean2d'].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/histmean2d_"+station+".pdf")
    c[station+'histchi22d'].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/histchi22d_"+station+".pdf")
        

        
for station in stationsAll :
    c[station+'MC'] = TCanvas(station+'MC'+'hist'+sys.argv[1],station+'MC'+'hist'+sys.argv[1])
    c[station+'MC'].Divide(2,2)
    for i,q in enumerate(['0','1','2','3']):
        c[station+'MC'].cd(i+1)
        c[station+'MC'].cd(i+1).SetLogy()
        hist[bxIDList[0]]['Cumul'][station][q][bestStep[station]].SetLineColor(kGreen)
        hist[bxIDList[0]]['Cumul'][station][q][bestStep[station]].Draw()
        hist[bxIDList[0]]['MCCumul'][station][q][bestStep[station]].SetLineColor(kBlue)
        hist[bxIDList[0]]['MCCumul'][station][q][bestStep[station]].Draw('same')
    c[station+'MC'].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/MC_"+station+".pdf")


for station in stationsAll :
    c[station+'Sig'] = TCanvas(station+'Sig'+'hist'+sys.argv[1],station+'Sig'+'hist'+sys.argv[1])
    c[station+'Sig'].Divide(2,2)
    for i,q in enumerate(['0','1','2','3']):
        c[station+'Sig'].cd(i+1)
        c[station+'Sig'].cd(i+1).SetLogy()
        #print hist[bxIDList[0]]['MC'][station][q][bestStep[station]].GetName()
        hist[bxIDList[0]]['MC'][station][q][bestStep[station]].SetLineColor(kRed)
        hist[bxIDList[0]]['MC'][station][q][bestStep[station]].Draw()
        hist[bxIDList[0]]['Sum'][station][q][bestStep[station]].SetLineColor(kBlue)
        hist[bxIDList[0]]['Sum'][station][q][bestStep[station]].Draw('same')
    c[station+"Sig"].Print("/calib/hrc/AnalysedRuns/"+runnumber+"/Sig_"+station+".pdf")



for station in stationsAll :
    c[station+'ROC'] = TCanvas(station+'ROC'+'hist'+sys.argv[1],station+'ROC'+'hist'+sys.argv[1])
    c[station+'ROC'].Divide(2,2)
    for i,q in enumerate(['0','1','2','3']):
        c[station+'ROC'].cd(i+1)
        hist[bxIDList[0]]['ROC'][station][q][bestStep[station]].Draw('APL')
        hist[bxIDList[0]]['ROC'][station][q][bestStep[station]].GetXaxis().SetTitle('Signal Efficiency')
        hist[bxIDList[0]]['ROC'][station][q][bestStep[station]].GetYaxis().SetTitle('Background Rejection')
        hist[bxIDList[0]]['ROC'][station][q][bestStep[station]].GetYaxis().SetRangeUser(0.8,1.02)
        hist[bxIDList[0]]['ROC'][station][q][bestStep[station]].GetXaxis().SetRangeUser(0.5,1.0)




f_out.cd()

for step in xrange(maxStep):
    for station in stationsAll :
        for i,q in enumerate(['0','1','2','3']):
            hist[bxIDList[0]]['ROC'][station][q][step].Write()

f_out.Close()

