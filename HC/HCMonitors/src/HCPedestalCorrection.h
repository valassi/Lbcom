#ifndef HCPEDESTALCORRECTION_H
#define HCPEDESTALCORRECTION_H 1

#include "TF2.h"
#include "TF1.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"


// Local
#include "HCMonitorBase.h"

/** @class HCPedestalCorrection HCPedestalCorrection.h
 *
 *
 */

class HCPedestalCorrection final : public HCMonitorBase {
 public:
  /// Standard constructor
  HCPedestalCorrection(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode finalize()   override;  ///< Algorithm finalization
  virtual StatusCode execute()    override;  ///< Algorithm execution

 private:
  /// TES location of HC digits.
  std::string m_digitLocation;
  std::vector<std::vector<std::vector<AIDA::IHistogram2D*> > > m_hCorrelation;
  AIDA::IHistogram1D* m_bxID = nullptr;
};
#endif
