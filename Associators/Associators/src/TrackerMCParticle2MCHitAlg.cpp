//
// This File contains the implementation of the MCParticle2MCHitAlg interface
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Authors: Rutger van der Eijk
//   Created: 3-7-2002

// from Event/MCEvent
#include "Event/MCParticle.h"
#include "Event/MCHit.h"

// from Event/LinkerEvent
#include "Linker/LinkerWithKey.h"
#include "Linker/LinkedFrom.h"

// local
#include "TrackerMCParticle2MCHitAlg.h"

DECLARE_COMPONENT( TrackerMCParticle2MCHitAlg )

using namespace LHCb;

TrackerMCParticle2MCHitAlg::TrackerMCParticle2MCHitAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty( "OutputData",  m_outputData =
                   "/Event/MC/Particles2MCTrackerHits" );
  declareProperty("Detectors", m_dets = {"Velo","IT","TT","OT","Muon"});
}

StatusCode TrackerMCParticle2MCHitAlg::execute()
{
  // get the MCParticles
  MCParticles* particles = get<MCParticles>(MCParticleLocation::Default );

  // save some typing
  typedef LinkedFrom<LHCb::MCHit,LHCb::MCParticle> HitLinks;

  // make the output table
  LinkerWithKey<MCParticle,MCHit> myLink( eventSvc(), msgSvc(), m_outputData );

  // get the sub-detector linkers
  std::vector<HitLinks> links;  links.reserve(m_dets.size());
  for( unsigned int idet = 0 ; idet< m_dets.size(); ++idet ) {
    std::string linkPath = MCParticleLocation::Default + "2MC" + m_dets[idet] + "Hits";
    links.emplace_back(evtSvc(),msgSvc(),linkPath);
    if (links.back().notFound())
      return Warning("Failed to find linker table", StatusCode::FAILURE);
  } // i

  // loop over MCParticles
  for(const auto& p : *particles) {
    for (auto& l : links) {
      for ( auto* aHit = l.first(p); aHit; aHit=l.next() ) {
        myLink.link(aHit,p);
      }
    }
  }

  if( msgLevel(MSG::DEBUG) ) debug() << "Linker table stored at " << m_outputData << endmsg;

  return StatusCode::SUCCESS;
}
