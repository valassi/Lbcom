// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Linker
#include "Linker/LinkerTool.h"

// MCEvent
#include "Event/MCTruth.h"
#include "Event/MCOTDeposit.h"
#include "Event/MCOTTime.h"

// Event
#include "Event/OTTime.h"

// local
#include "OTTime2MCDepositLinker.h"

/** @file OTTime2MCDepositLinker.cpp
 *
 *  Implementation of the OTTime2MCDepositLinker class
 *
 *  @author J. van Tilburg and Jacopo Nardulli
 *  @date   15/06/2004
 */

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OTTime2MCDepositLinker )

OTTime2MCDepositLinker::OTTime2MCDepositLinker( const std::string& name,
						ISvcLocator* pSvcLocator )
  : GaudiAlgorithm (name,pSvcLocator)
{
  declareProperty( "OutputData", m_outputData = LHCb::OTTimeLocation::Default + "2MCDeposits" );
  declareProperty( "acceptTime", m_acceptTime  = 7.8*Gaudi::Units::ns );
}


StatusCode OTTime2MCDepositLinker::execute()
{
  LHCb::OTTimes* timeCont = get<LHCb::OTTimes>( LHCb::OTTimeLocation::Default );
  LHCb::MCOTTimes* mcTime = get<LHCb::MCOTTimes>( LHCb::MCOTTimeLocation::Default );

  StatusCode sc = setMCTruth( timeCont, mcTime );
  if ( !sc.isSuccess() ) return Error( "Failed to set the mc truth link", sc );

  LinkerWithKey<LHCb::MCOTDeposit,LHCb::OTTime> myLink( evtSvc(), msgSvc(), outputData() );

  // loop and link OTTimes to MC truth

  for (const auto& time : *timeCont) {
    std::vector<LHCb::MCOTDeposit*> depVec;
    sc = associateToTruth( time, depVec );
    if ( !sc.isSuccess() ) return Error( "Failed to associate to truth" , sc );
    for (const auto& dep : depVec ) myLink.link(time, dep );
  } // loop iterTime

  return StatusCode::SUCCESS;
}

StatusCode OTTime2MCDepositLinker::associateToTruth( const LHCb::OTTime* aTime,
 						     std::vector<LHCb::MCOTDeposit*>& depVec )
{
  // Link time to truth
  const LHCb::MCOTTime* mcTime = mcTruth<LHCb::MCOTTime>( aTime );

  if (mcTime) {
    // link to deposits
    SmartRefVector<LHCb::MCOTDeposit> depCont = mcTime->deposits();
    if ( depCont.empty() ){
      error() << " Deposits Size" << depCont.size() << endmsg;
      return StatusCode::FAILURE;
    } // if depCont.empty()

    auto tdcTime = aTime->tdcTime();
    std::copy_if( depCont.begin(), depCont.end(),
                  std::back_inserter(depVec),
                  [&](const LHCb::MCOTDeposit* t) { return t->tdcTime() < (tdcTime + m_acceptTime); } );
  }
  return StatusCode::SUCCESS;
}

