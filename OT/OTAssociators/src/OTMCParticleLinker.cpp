// Include files

// from MCEvent
#include "Event/MCHit.h"
#include "Event/MCOTTime.h"

//from Event
#include "Event/OTTime.h"

// from Kernel
#include "Kernel/OTChannelID.h"

#include <Associators/Location.h>

// local
#include "OTMCParticleLinker.h"

//-----------------------------------------------------------------------------
// Implementation file for class : OTMCParticleLinker
//
// 2007-07-02 : Jan Amoraal
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OTMCParticleLinker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OTMCParticleLinker::OTMCParticleLinker( const std::string& name,
                                        ISvcLocator* svcLoc)
: Linker(name, svcLoc,
         {KeyValue{"MCOTTimeLocation", LHCb::MCOTTimeLocation::Default},
          KeyValue{"InputLocation",
                   Links::location(LHCb::OTTimeLocation::Default+"2MCHits")}},
         KeyValue{"OutputLocation", Links::location(LHCb::OTTimeLocation::Default)})
{
}

//=============================================================================
// main operator
//=============================================================================
LHCb::LinksByKey OTMCParticleLinker::operator()(const LHCb::MCOTTimes& mcTimes,
                                                const LHCb::LinksByKey& links) const
{
   /// Create linker instance; link channelID(key) to MCParticle
   auto output = outputLinks<LHCb::MCParticle>();
   auto channelToHit = inputLinks<LHCb::MCHit>(links);

   for (const auto mcTime : mcTimes) {
      for (auto entry : channelToHit.from(mcTime)) {
         auto particle = entry.to()->mcParticle();
         if (!particle) continue;
         output.link(mcTime, particle);
      }
   }
   return output;
}
