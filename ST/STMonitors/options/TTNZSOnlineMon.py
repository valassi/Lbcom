from Gaudi.Configuration import *
# Job options to configure the data on demand service for decoding the RawEvent

MoniTTNZSSeq = GaudiSequencer( 'MoniTTNZSSeq' )
MoniTTNZSSeq.MeasureTime = True
#ApplicationMgr().TopAlg.append( MoniTTNZSSeq )

from Configurables import (STNZSMonitor, ST__STNoiseCalculation, ST__STNoiseCalculationTool,
                           ST__STOnlineNoiseCalculationTool )

#==============================================================================
# Raw noise calculation...
#==============================================================================
ttNoiseCalc = ST__STNoiseCalculation("TTNoiseCalculation")
ttNoiseCalc.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttNoiseCalc.NoiseToolName = "TTNoiseCalculationTool"
ttNoiseCalc.OutputLevel = 3;

toolCMS = ST__STOnlineNoiseCalculationTool("TTNoiseCalculationTool")
toolCMS.DetType = "TT"
toolCMS.FollowPeriod = 2000
toolCMS.OutputLevel = 3
toolCMS.CondPath = "TTCondDB"
toolCMS.CountRoundRobin = True

ttRawNoiseMonitor = STNZSMonitor("TTRawNoiseMonitor")
ttRawNoiseMonitor.DetType = "TT"
ttRawNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttRawNoiseMonitor.NoiseToolName = "TTNoiseCalculationTool"
ttRawNoiseMonitor.UpdateRate = 1
ttRawNoiseMonitor.UpdateSummaryRate = 100
ttRawNoiseMonitor.OutputLevel = 3
#ttRawNoiseMonitor.UseODINTime = True
ttRawNoiseMonitor.UseRawNoise = True
ttRawNoiseMonitor.UseSourceID = False

ttPedSubNoiseMonitor = STNZSMonitor("TTPedSubNoiseMonitor")
ttPedSubNoiseMonitor.DetType = "TT"
ttPedSubNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttPedSubNoiseMonitor.NoiseToolName = "TTNoiseCalculationTool"
ttPedSubNoiseMonitor.UpdateRate = 1
ttPedSubNoiseMonitor.UpdateSummaryRate = 100
ttPedSubNoiseMonitor.OutputLevel = 3
#ttPedSubNoiseMonitor.UseODINTime = True
#ttRawNoiseMonitor.UseRawNoise = False
ttPedSubNoiseMonitor.UsePedSubNoise = True
ttPedSubNoiseMonitor.UseSourceID = False

ttCMSMonitor = STNZSMonitor("TTCMSNoiseMonitor")
ttCMSMonitor.DetType = "TT"
ttCMSMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
ttCMSMonitor.NoiseToolName = "TTNoiseCalculationTool"
ttCMSMonitor.UpdateRate = 1
ttCMSMonitor.UpdateSummaryRate = 100
ttCMSMonitor.OutputLevel = 3
#ttCMSMonitor.UseODINTime = True
ttCMSMonitor.UseSourceID = False

MoniTTNZSSeq.Members.append(ttNoiseCalc)
MoniTTNZSSeq.Members.append(ttRawNoiseMonitor)
MoniTTNZSSeq.Members.append(ttPedSubNoiseMonitor)
MoniTTNZSSeq.Members.append(ttCMSMonitor)
