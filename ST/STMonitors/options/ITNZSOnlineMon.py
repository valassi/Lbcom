from Gaudi.Configuration import *
# Job options to configure the data on demand service for decoding the RawEvent

MoniITNZSSeq = GaudiSequencer( 'MoniITNZSSeq' )
MoniITNZSSeq.MeasureTime = True
#ApplicationMgr().TopAlg.append( MoniITNZSSeq )

from Configurables import (STNZSMonitor, ST__STNoiseCalculation, ST__STNoiseCalculationTool,
                           ST__STOnlineNoiseCalculationTool )

#==============================================================================
# Raw noise calculation...
#==============================================================================
itNoiseCalc = ST__STNoiseCalculation("ITNoiseCalculation")
itNoiseCalc.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itNoiseCalc.NoiseToolName = "ITNoiseCalculationTool"
itNoiseCalc.OutputLevel = 3;

toolCMS = ST__STOnlineNoiseCalculationTool("ITNoiseCalculationTool")
toolCMS.DetType = "IT"
toolCMS.FollowPeriod = 2000
toolCMS.OutputLevel = 3
toolCMS.CondPath = "ITCondDB"
toolCMS.CountRoundRobin = True

itRawNoiseMonitor = STNZSMonitor("ITRawNoiseMonitor")
itRawNoiseMonitor.DetType = "IT"
itRawNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itRawNoiseMonitor.NoiseToolName = "ITNoiseCalculationTool"
itRawNoiseMonitor.UpdateRate = 1
itRawNoiseMonitor.UpdateSummaryRate = 100
itRawNoiseMonitor.OutputLevel = 3
#itRawNoiseMonitor.UseODINTime = True
itRawNoiseMonitor.UseRawNoise = True
itRawNoiseMonitor.UseSourceID = False

itPedSubNoiseMonitor = STNZSMonitor("ITPedSubNoiseMonitor")
itPedSubNoiseMonitor.DetType = "IT"
itPedSubNoiseMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itPedSubNoiseMonitor.NoiseToolName = "ITNoiseCalculationTool"
itPedSubNoiseMonitor.UpdateRate = 1
itPedSubNoiseMonitor.UpdateSummaryRate = 100
itPedSubNoiseMonitor.OutputLevel = 3
#itPedSubNoiseMonitor.UseODINTime = True
#itRawNoiseMonitor.UseRawNoise = False
itPedSubNoiseMonitor.UsePedSubNoise = True
itPedSubNoiseMonitor.UseSourceID = False

itCMSMonitor = STNZSMonitor("ITCMSNoiseMonitor")
itCMSMonitor.DetType = "IT"
itCMSMonitor.NoiseToolType = "ST::STOnlineNoiseCalculationTool"
itCMSMonitor.NoiseToolName = "ITNoiseCalculationTool"
itCMSMonitor.UpdateRate = 1
itCMSMonitor.UpdateSummaryRate = 100
itCMSMonitor.OutputLevel = 3
#itCMSMonitor.UseODINTime = True
itCMSMonitor.UseSourceID = False

MoniITNZSSeq.Members.append(itNoiseCalc)
MoniITNZSSeq.Members.append(itRawNoiseMonitor)
MoniITNZSSeq.Members.append(itPedSubNoiseMonitor)
MoniITNZSSeq.Members.append(itCMSMonitor)
