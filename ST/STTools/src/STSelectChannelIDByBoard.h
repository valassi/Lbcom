#ifndef STSelectChannelIDByBoard_H
#define STSelectChannelIDByBoard_H 1

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "Kernel/ISTChannelIDSelector.h"

/** @class STSelectChannelIDByBoard STSelectChannelIDByBoard.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author M.Needham
 *  @date   3/2/2009
 */

namespace LHCb{
  class STChannelID;
}

class STSelectChannelIDByBoard: public ST::ToolBase,
                         virtual public ISTChannelIDSelector {

 public:

  /// constructer
  STSelectChannelIDByBoard( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );
  /// initialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to ST cluster object to be selected
  *  @return true if cluster is selected
  */
  bool select     ( const LHCb::STChannelID& id ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to ST cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator() ( const LHCb::STChannelID& id ) const override;

 private:

  STSelectChannelIDByBoard() = delete;
  STSelectChannelIDByBoard (const STSelectChannelIDByBoard& ) = delete;
  STSelectChannelIDByBoard& operator= (const STSelectChannelIDByBoard& ) = delete;

  std::vector<unsigned int> m_boards;
  std::vector<unsigned int> m_sectors;

};

#endif // STSelectChannelIDByBoard_H
