#ifndef STCLUSTERKILLER_H
#define STCLUSTERKILLER_H 1

#include "Kernel/STAlgBase.h"
#include "Event/STCluster.h"

#include <string>

/** @class STClusterKiller STClusterKiller.h
 *
 *  Class for killing clusters 
 *
 *  @author M.Needham
 *  @date   06/10/2007
 */

struct ISTClusterSelector;

namespace LHCb{
  class STChannelID;
}

class STClusterKiller :public ST::AlgBase {

public:
  
  // Constructor
  STClusterKiller( const std::string& name, ISvcLocator* pSvcLocator); 

  // IAlgorithm members
  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

private:
  
  void removedClusters(const LHCb::STClusters* clusterCont,
                       std::vector<LHCb::STChannelID>& deadClusters) const;

   // smart interface to generator

  std::string m_selectorType;
  std::string m_selectorName;
  ISTClusterSelector* m_clusterSelector = nullptr;

  std::string m_inputLocation;

};

#endif // STCLUSTERKILLER_H
