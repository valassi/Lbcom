// GaudiKernel
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/Timing.h>

#include "TH1D.h"

// GaudiUtils
#include <GaudiUtils/HistoStats.h>
#include <GaudiUtils/HistoLabels.h>
#include <GaudiUtils/Aida2ROOT.h>

// Event
#include "Kernel/MuonTileID.h"
#include <Event/Particle.h>
#include <Event/RawBank.h>
#include <Event/RecVertex.h>
#include <Event/RawEvent.h>
#include "Event/ODIN.h"
#include <Event/L0DUReport.h>

// Lumi
#include "Event/LumiCounters.h"
#include "Event/LumiMethods.h"

// Detector
#include <DetDesc/Condition.h>

// STL
#include <string>
#include <vector>
#include <utility>
#include <math.h>
#include <fstream>
#include <sstream>
#include <time.h>

// boost
#include <boost/foreach.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

// local
#include "L0MuonAsymMonitor.h"

using LHCb::RawBank;
using LHCb::RawEvent;

using boost::assign::list_of;

//-----------------------------------------------------------------------------
// Implementation file for class : L0MuonAsymMonitor
//
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( L0MuonAsymMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
L0MuonAsymMonitor::L0MuonAsymMonitor( const std::string& name, ISvcLocator* pSvcLocator)
: GaudiHistoAlg ( name , pSvcLocator )
  , m_confTool(NULL)
  , m_itck(-1)
{

  declareProperty( "Input"  , m_input = "Phys/JpsiDQMonitorSel/Particles");

  declareProperty( "L0DUConfigProviderName"  , m_configName = "L0DUConfig");
  declareProperty( "L0DUConfigProviderType"  , m_configType = "L0DUMultiConfigProvider");

  declareProperty( "L0MuonPtADCThreshold" , m_l0threshold = -1);
  declareProperty( "L0MuonPtADCCutOnTIS"  , m_l0cut       = 40);
  declareProperty( "L0MuonPtADCCutOnTIS2" , m_l0cut2      = 30);
  declareProperty( "L0MuonADC2MeV"        , m_dac         = 50.);

  declareProperty( "L0MuonM1M2LUT",  m_M1M2LUTfile  ="");
  declareProperty( "L0MuonM1M2LUT2", m_M1M2LUTfile2 ="");

  declareProperty( "Online", m_online=true);

  declareProperty( "FillLUT", m_fillLUT=true);
  declareProperty( "UpdateFrequency" , m_updateFrequency = -1);
  declareProperty( "FillLUTDirectory", m_FillLUTDirectory= "");

  declareProperty( "JpsiMassCutMin"    , m_jpsimass_min = 3060.);
  declareProperty( "JpsiMassCutMax"    , m_jpsimass_max = 3140.);
  declareProperty( "SideBandMassCutMin", m_sidemass_min = 3000.);
  declareProperty( "SideBandMassCutMax", m_sidemass_max = 3200.);

  declareProperty( "L0CandLoc", m_l0candLoc ="Trig/L0/MuonBCSU");

  declareProperty( "Test", m_test =false);

}
//=============================================================================
// Initialization
//=============================================================================
StatusCode L0MuonAsymMonitor::initialize()
{
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first

  debug() <<"OK: initialize"<<endmsg;

  m_jpsimass = book1D("Jpsi invariant mass",m_sidemass_min,m_sidemass_max,50);
  m_jpsimass_peak = book1D("Jpsi invariant mass (peak)",m_sidemass_min,m_sidemass_max,50);

  // Book histos
  for (int icat=0; icat<2; ++icat) { // loop over categories (peak or side-bands)
    std::stringstream categorie("");
    if (-1+icat*2>0) categorie << "-Peak";
    else             categorie << "-SideBands";

    for (int index=0; index<2; ++index) { // loop over charge
      std::stringstream charge("");
      if (-1+index*2>0) charge << "-MuPos";
      else              charge << "-MuNeg";

      m_tosmuons[icat][index]            = book1D("TOS_muons"            +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuons_nomatching[icat][index] = book1D("TIS_muons_nomatching" +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuons[icat][index]            = book1D("TIS_muons"            +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuonscut[icat][index]         = book1D("TIScut_muons"         +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuonscutnew[icat][index]      = book1D("TIScutnew_muons"      +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuonsnewempty[icat][index]    = book1D("TISnewempty_muons"    +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuonscutnew2[icat][index]     = book1D("TIScutnew2_muons"     +charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tismuonsnew2empty[icat][index]   = book1D("TISnew2empty_muons"   +charge.str()+categorie.str(),0.,150*m_dac,15);

      m_tisptres[icat][index] = bookProfile1D("InvPtdiff"+charge.str()+categorie.str(),0.,150*m_dac,15);
      m_tisptresnew[icat][index] = bookProfile1D("InvPtnewdiff"+charge.str()+categorie.str(),0.,150*m_dac,15);

    } // end of loop over charge

    m_tisl0pt[icat]        = book1D("TIS_muons_l0pt"+categorie.str(),-0.5,127*m_dac+0.5,128);
    m_tisl0ptnew[icat]     = book1D("TIS_muons_l0ptnew"+categorie.str(),-0.5,127*m_dac+0.5,128);
    m_tisl0ptvspt[icat]    = book2D("L0pt_vs_pt"+categorie.str(),0.,10000.,20,-0.5,127*m_dac+0.5,128);
    m_tisl0ptnewvspt[icat] = book2D("L0ptnew_vs_pt"+categorie.str(),0.,10000.,20,-0.5,127*m_dac+0.5,128);

  } // end of loop over categories

  // Fill LUT from files
  if (m_M1M2LUTfile.size()>0) {
    bool success = openLUT(m_M1M2LUTfile,m_m2m1lut);
    if (! success)
      error()<<"Failed to read "<<m_M1M2LUTfile<<endmsg;
    else
      info() << m_M1M2LUTfile<<" read successfully, M1M2LUT filled with "<<m_m2m1lut.size()<<" M2 pads"<<endmsg;
  }
  if (m_M1M2LUTfile2.size()>0) {
    bool success = openLUT(m_M1M2LUTfile2,m_m2m1lut2);
    if (! success)
      error()<<"Failed to read "<<m_M1M2LUTfile2<<endmsg;
    else
      info() << m_M1M2LUTfile2<<" read successfully, M1M2LUT filled with "<<m_m2m1lut2.size()<<" M2 pads"<<endmsg;
  }

  // Check
  m_counter = 0;
  for (int i=0; i<4; ++i) m_tobefilled[i] = m1m2PadsPtSumMap();
  if (m_fillLUT) {
    if (m_FillLUTDirectory.size()==0){
      error() <<"FillLUTDirectory is not defined, LUT will not be filled"<<endmsg;
      m_fillLUT = false;
    } else if (! boost::filesystem::exists(m_FillLUTDirectory)) {
      error() <<"FillLUTDirectory doest not exist, LUT will not be filled"<<endmsg;
      m_fillLUT = false;
    } else {
      info() <<"LUT will be filled and save in "<<m_FillLUTDirectory<<endmsg;
    }
  }
  m_firstRun = -1;
  m_currentRun = -1;
  m_startTime = getTime();

  if (m_test){
    if (m_FillLUTDirectory.size()>0) {
      std::string filename = m_FillLUTDirectory+"/test.txt";;
      std::ofstream testfile(filename.c_str());
      if (testfile.is_open()){
        testfile << getTime() <<"\n";
        testfile.close();
      } else {
        error() <<"Could not write test file in "<<m_FillLUTDirectory<<endmsg;
      }
    }
  }

  m_confTool = tool<IL0DUConfigProvider>(m_configType , m_configName );
  m_updateTCK = ( m_l0threshold < 0 );

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode L0MuonAsymMonitor::execute()
{
  StatusCode sc = StatusCode::SUCCESS;

  debug() <<"OK: execute"<<endmsg;

  if (m_updateTCK) {
    sc = updateL0MuonThreshold();
    if (sc==StatusCode::FAILURE)
      return Error("Unable to load the new TCK configuration",StatusCode::SUCCESS,20);
  }

  sc = monitorAsym();
  if (sc==StatusCode::FAILURE)
    return Error("Error in the L0Muon asymmetry monitoring",StatusCode::SUCCESS,20);

  setFilterPassed(true);
  return sc;
}


//=============================================================================
//  Finalize
//=============================================================================
StatusCode L0MuonAsymMonitor::finalize()
{
  if (m_fillLUT) { // Fill LUT
    int success = writeLUT();
    if (!success) {
      error()<<"Failed to write LUT"<<endmsg;
    }
  }

  if (!m_online)
  {
    // TH1D * h_tismuonscut[2][2];
    // TH1D * h_tismuons[2][2];
    // TH1D * h_effmuons[2][2];
    // TH1D * h_asymmuons[2];

    for (int icat=0; icat<2; ++icat) { // loop over categories (peak or side-bands)
      std::string categorie = "-Peak";
      if (-1+icat*2<0) categorie="-SideBands";
      for (int index=0; index<2; ++index) {
        h_tismuonscut[icat][index] = Gaudi::Utils::Aida2ROOT::aida2root(m_tismuonscut[icat][index]);
        h_tismuons[icat][index]    = Gaudi::Utils::Aida2ROOT::aida2root(m_tismuons[icat][index]);
      }

      for (int index=0; index<2; ++index) {
        std::string sign = "-MuPos";
        if (-1+index*2<0) sign = "-MuNeg";
        std::string name = "L0Muon efficiency -vs- L0-PT"+sign+categorie;
        h_effmuons[icat][index] = (TH1D*) h_tismuonscut[icat][index]->Clone(name.c_str());
        h_effmuons[icat][index]->SetTitle( name.c_str() );
        h_effmuons[icat][index]->Sumw2();
        bool ok = h_effmuons[icat][index]->Divide(h_effmuons[icat][index],h_tismuons[icat][index],1.,1.,"B");
        if (!ok) debug()<<"Pb with the division "<<endmsg;
      }
      h_asymmuons[icat] = (TH1D*) h_effmuons[icat][0]->GetAsymmetry( h_effmuons[icat][1] ) ;
      std::string title = "L0Muon efficiency charge asymmetry"+categorie;
      h_asymmuons[icat]->SetTitle( title.c_str() );

      // for (int index=0; index<2; ++index) {
      //   m_effmuons[icat][index] =  NOT::EXISTING::ROOT2Aida (h_effmuons[icat][index]);
      // }
      // m_asymmuons[icat] = NOT::EXISTING::ROOT2Aida(h_asymmuons[icat]);

    } // end of loop over categories

  }

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//==============================================================================
StatusCode L0MuonAsymMonitor::monitorAsym()
{

  // Retrieve L0MuonCandidates
  const LHCb::L0MuonCandidates* l0candidates = getIfExists<LHCb::L0MuonCandidates>( m_l0candLoc );
  if( NULL==l0candidates )     return Warning("L0MuonCandidates not found",StatusCode::SUCCESS,10);
  if (l0candidates->size()==0) return Warning("No L0MuonCandidates",StatusCode::SUCCESS,10);

  // Get the input particles (Jpsi)
  LHCb::Particle::Range particles = getIfExists<LHCb::Particle::Range>(m_input);
  BOOST_FOREACH( const LHCb::Particle* jpsi, particles ) {

    // Containers for selected matched muons
    float selected_muons_pt[2];
    selected_muons_pt[0]=-1;
    selected_muons_pt[1]=-1;
    LHCb::L0MuonCandidates::const_iterator l0matched_muons_l0cand[2];
    l0matched_muons_l0cand[0]=l0candidates->end();
    l0matched_muons_l0cand[1]=l0candidates->end();

    // Loop over particle's daughters (muons)
    bool muon_filter = true;
    BOOST_FOREACH( const LHCb::Particle* muon, jpsi->daughters() ){

      // Check that the pid corresponds to a muon
      int abspid  = (muon->particleID()).abspid() ;
      if (abspid!=13) {
        counter("Muon pid is wrong")++;
        muon_filter = false;
        break;
      }

      // Get MuonTileIDs associated to the particle's track
      std::vector< LHCb::MuonTileID>  muonIDs;
      getMuonPartTiles(muonIDs,muon);
      if (muonIDs.size()==0) {
        counter("Muon does not have associated MuonTileIDs")++;
        muon_filter = false;
        break;
      }

      // Fill container for selected matched muons
      double pt  = muon->pt();
      int   ch   = muon->charge();
      int index = (ch+1)/2;
      selected_muons_pt[index] = pt;

      // Matching to L0Candidates : Loop over L0Candidates
      for (LHCb::L0MuonCandidates::const_iterator itcand= l0candidates->begin();
           itcand<l0candidates->end();++itcand) {

        int l0match=0;
        for (int sta=0; sta<3; ++sta){

          std::vector<LHCb::MuonTileID> l0pads = (*itcand)->muonTileIDs(sta);

          for (std::vector<LHCb::MuonTileID>::iterator itl0pad = l0pads.begin();
               itl0pad<l0pads.end();++itl0pad){

            std::vector< LHCb::MuonTileID>::iterator itmatch = std::find(muonIDs.begin(),
                                                                         muonIDs.end(), *itl0pad);
            if ( itmatch!=muonIDs.end() ) {
              ++l0match;
              break; // next station
            }

          } // - L0candidate pads in station
        } // - stations

        if (l0match>=2){ // if muon is matched to a L0-candidate
          l0matched_muons_l0cand[index] = itcand;
          break; // leave l0candidate loop
        } // end if muon is matched to a L0-candidate
      } // end of loop over L0candidates
    } // end of loop over daughters (muons)

    if (!muon_filter) {
      counter("J/Psi failing muon criteria")++;
      continue;
    }

    counter("J/Psi satisfying muon criteria")++;
    double mass = jpsi->measuredMass ();
    fill(m_jpsimass,mass,1.);

    // Filter on J/Psi Mass
    if ( (m_sidemass_min>mass)||(mass>m_sidemass_max) ) continue;
    counter("J/Psi in considered mass range")++;

    // Categorize with J/Psi mass
    int im_cat = 1;
    if ( (m_jpsimass_min<mass)&&(mass<m_jpsimass_max) ) {
      im_cat = 0;
      fill(m_jpsimass_peak,mass,1.);
      counter("J/Psi in mass peak")++;
    }

    // Loop over muon pair
    for (int index=0 ; index<2; ++index){

      // Is this muon matched to a L0 candidate ?
      float pt = selected_muons_pt[index];
      LHCb::L0MuonCandidates::const_iterator l0cand = l0matched_muons_l0cand[index];
      if (l0cand==l0candidates->end()) continue; // muon is not matched -> next muon

      // Is the matched L0candidate pt above threshold ?
      int ipt    = (*l0cand)->encodedPt();
      int encoded_l0pt   = (ipt&0x7F); //
      if (encoded_l0pt<m_l0threshold) continue; // muon is not TOS -> next muon

      // We have a TOS muon -> fill histo
      fill(m_tosmuons[im_cat][index],pt,1.);

      // Since this muon is TOS, the other is TIS.
      int other_index = (index+1)%2; // other muon index
      float pt_tis = selected_muons_pt[other_index];
      fill(m_tismuons_nomatching[im_cat][other_index],pt_tis,1.);

      // Is this TIS muon matched to a L0candidate ?
      LHCb::L0MuonCandidates::const_iterator l0cand_tis = l0matched_muons_l0cand[other_index];
      if (l0cand_tis!=l0candidates->end()) { // Other (TIS) muon is matched

        int ipt_tis    = (*l0cand_tis)->encodedPt();
        int encoded_l0pt_tis = (ipt_tis&0x7F);
        float l0pt_tis       = encoded_l0pt_tis*m_dac;
        fill(m_tismuons[im_cat][other_index],pt_tis,1.);
        if (encoded_l0pt_tis>=m_l0cut) {
          fill(m_tismuonscut[im_cat][other_index],pt_tis,1.);
        }
        fill(m_tisl0pt[im_cat],l0pt_tis,1.);
        fill(m_tisl0ptvspt[im_cat],pt_tis,l0pt_tis,1.);
        fill(m_tisptres[im_cat][other_index],pt_tis,(1./l0pt_tis)-(1./pt_tis),1.);

        // Get candidate pads for LUTs
        LHCb::MuonTileID seed;
        LHCb::MuonTileID m2pad;
        LHCb::MuonTileID m1pad;
        getCandidatePads(*l0cand_tis,seed,m2pad,m1pad);

        // Check other thresholds and/or other LUTs
        int newipt = ipt_tis;
        if (m_m2m1lut.size()>0) { // recompute the pt using a different LUT
          newipt = getPTFromLUT(m2pad, m1pad, m_m2m1lut);
          if (newipt<=0) fill(m_tismuonsnewempty[im_cat][other_index],pt_tis,1.);
        }
        if (newipt>0) { // fill histo to get efficiency w/ a new threshold
          int encoded_newl0pt = (newipt&0x7F);
          if (encoded_newl0pt>=m_l0cut) fill(m_tismuonscutnew[im_cat][other_index],pt_tis,1.);
          float newl0pt = encoded_newl0pt*m_dac;
          fill(m_tisl0ptnew[im_cat],newl0pt,1.);
          fill(m_tisl0ptnewvspt[im_cat],pt_tis,newl0pt,1.);
          fill(m_tisptresnew[im_cat][other_index],pt_tis,(1./newl0pt)-(1./pt_tis),1.);
        }

        // Check yet another thresholds and/or other LUTs
        if (m_m2m1lut2.size()>0) { // recompute the pt using a different LUT
          newipt = getPTFromLUT(m2pad, m1pad, m_m2m1lut2);
          if (newipt<=0) fill(m_tismuonsnew2empty[im_cat][other_index],pt_tis,1.);
        }
        if (newipt>0) {  // fill histo to get efficiency w/ a new threshold
          int encoded_newl0pt = (newipt&0x7F);
          if (encoded_newl0pt>=m_l0cut2) fill(m_tismuonscutnew2[im_cat][other_index],pt_tis,1.);
        }

        if (m_fillLUT && (im_cat==0) ) { // Fill LUT
          fillLUT(m2pad,m1pad,pt_tis);
          ++m_counter;
          if (m_counter==m_updateFrequency){
            int success = writeLUT();
            if (success) {
              m_counter=0;
              m_firstRun = m_currentRun;
              m_startTime = getTime();
            } else {
              error()<<"Failed to write LUT ... LUT filling will be disabled"<<endmsg;
              m_fillLUT = false;
            }
          }
        } // end fill LUT

      } // end other (TIS) muon is matched
    } // end of loop over muons pair
  } // end of loop over candidate (jpsi)

  return StatusCode::SUCCESS;

}

//==============================================================================
bool L0MuonAsymMonitor::openLUT(std::string & filename, m2m1Lut & m1m2_lut)
{

  std::ifstream lutfile(filename.c_str());

  if (lutfile.is_open()){
    std::string line;

    while (lutfile.good()){

      getline(lutfile,line);
      std::istringstream isline(line);
      std::string sid;
      isline >> sid;

      LHCb::MuonTileID m2pad = LHCb::MuonTileID(sid);

      m1ptMap m1pt;
      do {
        std::string s_m1pad;
        int ipt;
        isline >> s_m1pad;
        isline >> ipt;
        m1pt[LHCb::MuonTileID(s_m1pad)]=ipt;
      } while (isline);

      m1m2_lut[m2pad]=m1pt;

    }
    lutfile.close();
  } else {
    return false;
  }
  return true;
}

//==============================================================================
void L0MuonAsymMonitor::getMuonPartTiles(std::vector< LHCb::MuonTileID> & muonIDs, const LHCb::Particle* muon)
{

  // Get the track associated to the muon particle
  const LHCb::ProtoParticle *proto = muon->proto();
  const LHCb::Track * track = proto->track () ;

  // Get the MuonTileIDs from the associated muon track
  LHCb::Track::Range muon_tracks = getIfExists<LHCb::Track::Range>("Rec/Track/Muon");
  BOOST_FOREACH(const LHCb::Track* mu_tr, muon_tracks)
  {
    BOOST_FOREACH(SmartRef<LHCb::Track> ancestor, mu_tr->ancestors () )
    {
      if (ancestor.target() == track)
      {
        BOOST_FOREACH(const LHCb::LHCbID& lhcbId, mu_tr->lhcbIDs() )
        {
          if (lhcbId.isMuon()) {
            muonIDs.push_back(lhcbId.muonID());
          }
        }
        return;
      }
    }
  }
  counter("No associated muon track found")++;
  return;
}


void L0MuonAsymMonitor::getCandidatePads(LHCb::L0MuonCandidate * l0candidate,
                                         LHCb::MuonTileID & seed, LHCb::MuonTileID & m2pad, LHCb::MuonTileID & m1pad)
{
  seed  = (l0candidate->muonTileIDs(2))[0];

  // get M2 pad used in the trigger
  std::vector<LHCb::MuonTileID> m2pads = (l0candidate->muonTileIDs(1));
  m2pad = m2pads[0];
  if (m2pad.region()<seed.region()) {
    LHCb::MuonTileID m2pad_Reg = m2pads[0];
    m2pad = m2pad_Reg.layout().tilesInRegion(m2pad_Reg,seed.region())[0];
  }
  if (!m2pad.isValid()){
    if (m2pad.nX()>=2*m2pad.layout().xGrid()) {
      std::vector< LHCb::MuonTileID > m2pads = m2pad.layout().tiles(m2pad);
      m2pad = m2pads[0];
    }
  }

  // get M1 pad used in the trigger
  std::vector<LHCb::MuonTileID> m1pads = (l0candidate->muonTileIDs(0));
  m1pad = m1pads[0];
  if (m1pad.region()<seed.region()) {
    LHCb::MuonTileID m1pad_Reg = m1pads[0];
    m1pad = m1pad_Reg.layout().tilesInRegion(m1pad_Reg,seed.region())[0];
  }
  if (!m1pad.isValid()){
    if (m1pad.nX()>=2*m1pad.layout().xGrid()) {
      std::vector< LHCb::MuonTileID > m1pads = m1pad.layout().tiles(m1pad);
      m1pad = m1pads[0];
    }
  }

}

//==============================================================================
int L0MuonAsymMonitor::getPTFromLUT( LHCb::MuonTileID m2pad, LHCb::MuonTileID m1pad,
                                     m2m1Lut & m1m2_lut)
{

  int ipt = -1;

  m2m1Lut::iterator itm1m2lut = m1m2_lut.find(m2pad);
  if (itm1m2lut !=  m1m2_lut.end()){
    m1ptMap m1pt = (*itm1m2lut).second;
    m1ptMap::iterator itm1pt = m1pt.find(m1pad);
    if (itm1pt != m1pt.end()) {
      ipt = (*itm1pt).second;
    } else {
      counter("M1 pad not in M1M2LUT")++;
    }
  } else {
    counter("M2 pad not in M1M2LUT")++;
  }
  return ipt;

}

//==============================================================================
void L0MuonAsymMonitor::fillLUT(LHCb::MuonTileID m2, LHCb::MuonTileID m1,  double pt)
{
  m1m2Pads m2m1(m2,m1);

  //m1m2PadsPtSumMap *lut = m_tobefilled+m2.quarter();

  m1m2PadsPtSumMap::iterator itlut = m_tobefilled[m2.quarter()].find(m2m1);

  ptSum ptsum;

  if (itlut==m_tobefilled[m2.quarter()].end()) {
    ptsum = ptSum();
    ptsum.push_back(pt);
    m1m2PadsPtSumEl new_el(m2m1,ptsum);
    m_tobefilled[m2.quarter()].insert(new_el);
  } else {
    (itlut->second).push_back(pt);
  }
}

bool L0MuonAsymMonitor::writeLUT()
{
  std::ostringstream osfilename;
  osfilename << m_FillLUTDirectory <<"/"<<"LUT_";
  osfilename << m_firstRun<<"_"<<m_currentRun;
  if (m_online) {
    osfilename << "_";
    osfilename << m_startTime;
    osfilename << "_";
    osfilename << getTime();
  }
  osfilename <<".txt";
  std::string filename = osfilename.str();
  std::ofstream lutfile(filename.c_str());
  if (lutfile.is_open()){

    for (int qua= 0; qua<4; ++qua){
      //m1m2PadsPtSumMap *lut = m_tobefilled+qua;
      for (m1m2PadsPtSumMap::iterator itlut = m_tobefilled[qua].begin();
           itlut!=m_tobefilled[qua].end(); ++itlut){
        m1m2Pads m1m2 = (*itlut).first;
        ptSum ptsum = (*itlut).second;
        std::ostringstream osline;
        osline<<(m1m2.first).toString()<<" "<<(m1m2.second).toString();
        BOOST_FOREACH( double pt, ptsum){
          osline<<" "<<pt;
        }
        osline<<"\n";
        lutfile<<osline.str();
      }
      m_tobefilled[qua].clear();
    }
    lutfile.close();
  } else {
    return false;
  }
  return true;
}

std::string L0MuonAsymMonitor::getTime()
{
  std::ostringstream osfilename;
  time_t rawtime;
  struct tm * timeinfo;
  time( &rawtime );
  timeinfo = localtime( &rawtime );
  osfilename << std::setw(4)<< (timeinfo->tm_year)+1900;
  osfilename << std::setw(2)<< std::setfill('0')<<(timeinfo->tm_mon)+1;
  osfilename << std::setw(2)<< std::setfill('0')<<timeinfo->tm_mday<<"_";
  osfilename << std::setw(2)<< std::setfill('0')<<timeinfo->tm_hour;
  osfilename << std::setw(2)<< std::setfill('0')<<timeinfo->tm_min;
  osfilename << std::setw(2)<< std::setfill('0')<<timeinfo->tm_sec;
  return osfilename.str();
}

StatusCode L0MuonAsymMonitor::updateL0MuonThreshold()
{

  LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default,false);
  if ( NULL == odin ) {
    return Warning("Can not access ODIN bank to get TCK",StatusCode::SUCCESS,20);
  }

  // read tck from odin
  unsigned int odintck = odin->triggerConfigurationKey();
  int itck = int(odintck&0xFFFF);

  if (m_itck == itck) return StatusCode::SUCCESS;

  m_itck = itck;

  info()<< "Loading configuration for TCK = 0x"<<std::hex << m_itck <<std::dec << endmsg;
  LHCb::L0DUConfig* l0duconfig  = m_confTool->config( m_itck );
  if( NULL == l0duconfig){
    error() << " Unable to load the configuration for TCK = 0x"<<std::hex << m_itck <<std::dec << endmsg;
    return Error(" Unable to load the TCK configuration",StatusCode::FAILURE,20);
  }

  const LHCb::L0DUElementaryCondition * cond = l0duconfig->elementaryCondition("Muon","Muon1(Pt)");
  m_l0threshold = cond->threshold();
  info()<< "Threshold for TCK = 0x"<<std::hex << m_itck <<std::dec << " is "<<m_l0threshold <<" (adc)"<<endmsg;

  return StatusCode::SUCCESS;
}

//==============================================================================
