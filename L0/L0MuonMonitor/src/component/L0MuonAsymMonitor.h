// $Id: L0MuonAsymMonitor.h,v 1.6 2010/06/02 08:14:47 jost Exp $
#ifndef COMPONENT_L0MuonAsymMonitor_H
#define COMPONENT_L0MuonAsymMonitor_H 1

// Include files
// from system
#include "math.h"
#include <map>
// from Gaudi

#include "TH1D.h"

#include "GaudiAlg/GaudiHistoAlg.h"
//for hltselreports
#include "GaudiKernel/DataObject.h"
// #include "Event/HltSelReports.h"
// #include "Event/HltDecReports.h"
#include "Event/L0MuonCandidate.h"

#include "GaudiKernel/IHistogramSvc.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/StringKey.h"
#include "GaudiKernel/ObjectContainerBase.h"

#include "L0Interfaces/IL0DUConfigProvider.h"

class L0MuonAsymMonitor : public GaudiHistoAlg

{
public:

  typedef std::map<LHCb::MuonTileID,int> m1ptMap;
  typedef std::map< LHCb::MuonTileID, m1ptMap > m2m1Lut;

  typedef std::vector<double> ptSum;
  typedef std::pair<LHCb::MuonTileID,LHCb::MuonTileID> m1m2Pads;
  typedef std::pair< m1m2Pads ,ptSum > m1m2PadsPtSumEl;
  typedef std::map< m1m2Pads ,ptSum > m1m2PadsPtSumMap;

public:
   /// Standard constructor
   L0MuonAsymMonitor( const std::string& name, ISvcLocator* pSvcLocator );

   StatusCode initialize() override;    ///< Algorithm initialization
   StatusCode execute() override;    ///< Algorithm execution
   StatusCode finalize() override;    ///< Algorithm finalization

private:

  StatusCode updateL0MuonThreshold();
  StatusCode monitorAsym();

  bool openLUT(std::string & filename, m2m1Lut & m1m2_lut);

  void getCandidatePads(LHCb::L0MuonCandidate * l0candidate, LHCb::MuonTileID &m3, LHCb::MuonTileID &m2, LHCb::MuonTileID &m1);
  int  getPTFromLUT( LHCb::MuonTileID m2, LHCb::MuonTileID m1,  m2m1Lut & m1m2_lut);
  void getMuonPartTiles(std::vector< LHCb::MuonTileID> & muonIDs, const LHCb::Particle* muon);

  void fillLUT(LHCb::MuonTileID m2, LHCb::MuonTileID m1,  double pt);
  bool writeLUT();

  m2m1Lut m_m2m1lut;
  m2m1Lut m_m2m1lut2;

  m1m2PadsPtSumMap m_tobefilled[4];
  int m_counter;
  unsigned int m_firstRun;
  std::string m_startTime;
  unsigned int m_currentRun;

  std::string getTime();

  IL0DUConfigProvider* m_confTool;
  bool m_updateTCK;

  int m_itck;

protected:

  AIDA::IHistogram1D * m_jpsimass;
  AIDA::IHistogram1D * m_jpsimass_peak;

  AIDA::IHistogram1D * m_tosmuons[2][2];
  AIDA::IHistogram1D * m_tismuons_nomatching[2][2];
  AIDA::IHistogram1D * m_tismuons[2][2];
  AIDA::IHistogram1D * m_tismuonscut[2][2];
  AIDA::IHistogram1D * m_tismuonscutnew[2][2];
  AIDA::IHistogram1D * m_tismuonsnewempty[2][2];
  AIDA::IHistogram1D * m_tismuonscutnew2[2][2];
  AIDA::IHistogram1D * m_tismuonsnew2empty[2][2];

  AIDA::IProfile1D * m_tisptres[2][2];
  AIDA::IProfile1D * m_tisptresnew[2][2];

  AIDA::IHistogram1D * m_tisl0pt[2];
  AIDA::IHistogram1D * m_tisl0ptnew[2];
  AIDA::IHistogram2D * m_tisl0ptvspt[2];
  AIDA::IHistogram2D * m_tisl0ptnewvspt[2];

  TH1D * h_tismuonscut[2][2];
  TH1D * h_tismuons[2][2];
  TH1D * h_effmuons[2][2];
  TH1D * h_asymmuons[2];

  bool m_online;

  std::string m_input;

  std::string m_configName; ///< L0DUConfigProviderName tool name
  std::string m_configType; ///< L0DUConfigProviderType tool name

  /// L0Muon threshold applied to test if muon is tos and to compute the efficiency,
  /// if negative, the threshold will be taken from the TCK
  int m_l0threshold;
  int m_l0cut;          ///< An L0Muon threshold use to compute the efficiency eventually with a new LUT
  int m_l0cut2;         ///< Yet an other L0Muon threshold use to compute the efficiency eventually with yet another LUT
  float m_dac;          ///< ADC -> MeV conversion factor


  std::string m_M1M2LUTfile;
  std::string m_M1M2LUTfile2;

  bool m_fillLUT;
  int m_updateFrequency;
  std::string m_FillLUTDirectory;

  float m_jpsimass_min;
  float m_jpsimass_max;
  float m_sidemass_min;
  float m_sidemass_max;

  std::string m_l0candLoc;

  bool m_test;

};
#endif // COMPONENT_L0MuonAsymMonitor_H
