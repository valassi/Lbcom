

2018-03-06 Lbcom v22r0
===

This version uses LHCb v44r0, Gaudi v29r3 and LCG_93 with ROOT 6.12.06.
<p>
This version is released on `2018-patches` branch.

Built relative to Lbcom v21r1, with the following changes:

### Enhancements
- Allow finding LHCb::Digit MC associations only by digit index, !191 (@apearce)

### Code modernisations and cleanups
- Fixed VeloAlgorithms to deal with change in gaudi/Gaudi!428, !186 (@cattanem)

  

