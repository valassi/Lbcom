################################################################################
# Package: VeloAssociators
################################################################################
gaudi_subdir(VeloAssociators v2r5p1)

gaudi_depends_on_subdirs(Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/VeloEvent
                         GaudiAlg)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(VeloAssociators
                 src/*.cpp
                 INCLUDE_DIRS Event/DigiEvent Event/VeloEvent
                 LINK_LIBRARIES LinkerEvent MCEvent GaudiAlgLib)

