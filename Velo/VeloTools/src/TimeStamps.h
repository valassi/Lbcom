#ifndef TIMESTAMPS_H 
#define TIMESTAMPS_H 1

#include <map>
#include <vector>

#include "GaudiAlg/GaudiTool.h"
#include "Tell1Kernel/ITimeStampProvider.h" 


namespace Velo
{
  /** @class TimeStamps TimeStamps.h
   *
   *  Implementation of TimeStampProvider interface.
   *
   *  Provides time stamps in the format
   *
   *    YYYY-MM-DD_HH.MM.SS
   *
   *  @author Kurt Rinnert
   *  @date   2009-08-21
   */
  class TimeStamps final : public GaudiTool, virtual public Velo::ITimeStampProvider {

    public: 
      /// Standard constructor
      TimeStamps( const std::string& type, 
          const std::string& name,
          const IInterface* parent);

      virtual StatusCode initialize() override; ///< Sets init time stamp

      virtual std::string initTime()  override; ///< time of initialisation

      virtual std::string now()       override; ///< current time

    private:

      // time stamps
      std::string m_initTime;

  };
}
#endif // TIMESTAMPS_H 
