#ifndef VELOSIMMONI_H
#define VELOSIMMONI_H 1

// Include files
#include "Event/MCVeloFE.h"
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "VeloDet/DeVelo.h"

/** @class VeloSimMoni VeloSimMoni.h
 *
 *
 *  @author Tomasz Szumlak & Chris Parkes
 *  @date   2005-11-03
 */

class MCVeloFE;
struct IMCVeloFEType;

class VeloSimMoni final : public GaudiTupleAlg {
public:
  /// Standard constructor
  VeloSimMoni( const std::string& name, ISvcLocator* pSvcLocator );

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode execute   () override;    ///< Algorithm execution
  virtual StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  void getData();
  void VeloFEMonitor();

private:

  bool m_printInfo;
  bool m_detailedMonitor;
  bool m_veloFEMoni;

  LHCb::MCVeloFEs* m_mcVFEsCont = nullptr;
  DeVelo* m_veloDet             = nullptr;
  IMCVeloFEType* m_feTypeTool   = nullptr;
  long m_nMCVeloFE              = 0;
  double m_nMCVeloFE2           = 0.;
  long m_nMCVeloFEs             = 0;
  long m_nMCVeloFEn             = 0;
  long m_nMCVeloFEo             = 0;
  long m_NumberOfEvents         = 0;

};
#endif // VELOSIMMONI_H
