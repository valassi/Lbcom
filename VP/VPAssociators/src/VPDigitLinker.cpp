#include <map>

#include <Event/MCHit.h>
#include <Event/MCVPDigit.h>
#include <Event/VPDigit.h>

#include <Associators/Location.h>

#include "VPDigitLinker.h"

DECLARE_COMPONENT(VPDigitLinker)

//=============================================================================
// Constructor
//=============================================================================
VPDigitLinker::VPDigitLinker(const std::string& name,
                             ISvcLocator* svcLoc)
: MultiLinker(name, svcLoc,
              {KeyValue{"DigitLocation", LHCb::VPDigitLocation::Default},
               KeyValue{"MCDigitLocation", LHCb::MCVPDigitLocation::Default}},
              {KeyValue{"HitLinkLocation", Links::location(LHCb::VPDigitLocation::Default + "2MCHits")},
               KeyValue{"ParticleLinkLocation", Links::location(LHCb::VPDigitLocation::Default)}})
{

}

//=============================================================================
// Execution
//=============================================================================
std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>
VPDigitLinker::operator()(const LHCb::VPDigits& digits,
                          const LHCb::MCVPDigits& mcdigits) const
{
  // Create association tables.
  auto hitLinks = outputLinks<LHCb::MCHit>();
  auto particleLinks = outputLinks<LHCb::MCParticle>();

  // Loop over the digits.
  for (const auto digit : digits) {
    // Get the MC digit with the same channel ID.
    const auto mcDigit = mcdigits.object(digit->key());
    if (!mcDigit) continue;
    // Get the MC hits associated to the MC digit and their weights.
    const auto& hits = mcDigit->mcHits();
    const auto& deposits = mcDigit->deposits();
    const unsigned int nDeposits = deposits.size();
    // Make a map of the MC hits/particles and weights.
    std::map<const LHCb::MCHit*, double> hitMap;
    std::map<const LHCb::MCParticle*, double> particleMap;
    for (unsigned int i = 0; i < nDeposits; ++i) {
      const double weight = deposits[i];
      const auto hit = hits[i];
      const auto particle = hit->mcParticle();
      // Check if the hit originates from a delta ray.
      if (particle && particle->originVertex() &&
          particle->originVertex()->type() == LHCb::MCVertex::DeltaRay) {

        // Check if there is another hit from the mother particle of the delta.
        const auto mother = particle->mother();
        auto mhit = std::find_if(hits.begin(), hits.end(),
                                 [&mother](const LHCb::MCHit* h) {
                                   return h->mcParticle()==mother;
                                 });
        if (mhit != hits.end()) {
          // Add the deposit to the hit of the mother particle.
          hitMap[*mhit] += weight;
          particleMap[(*mhit)->mcParticle()] += weight;
        } else {
          // If no other hit from the mother particle is found,
          // add the delta ray hit as a separate entry.
          hitMap[hit] += weight;
          particleMap[particle] += weight;
        }
      } else {
        hitMap[hit] += weight;
        particleMap[particle] += weight;
      }
    }
    for (const auto& element : hitMap) {
      // Make the association.
      hitLinks.link(digit->key(), element.first, element.second);

    }
    for (const auto& element : particleMap) {
      // Make the association.
      particleLinks.link(digit->key(), element.first, element.second);
    }
  }
  return std::make_tuple(std::move(hitLinks), std::move(particleLinks));
}
