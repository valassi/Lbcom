#ifndef VPCLUSTER2MCPARTICLELINKER_H
#define VPCLUSTER2MCPARTICLELINKER_H 1

// LHCb
#include <Event/MCParticle.h>
#include <Event/LinksByKey.h>
#include <Event/VPLightCluster.h>

// Associators
#include <Associators/Linker.h>

/** @class VPClusterLinker VPClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC particles, based on the association tables
 * for individual pixels produced by VPDigitLinker.
 */

class VPCluster2MCParticleLinker
   : public Gaudi::Functional::Linker<LHCb::LinksByKey(const std::vector<LHCb::VPLightCluster>&,
                                                       const LHCb::LinksByKey&)>
{
public:

   //Standard constructor
   VPCluster2MCParticleLinker(const std::string& name, ISvcLocator* pSvcLocator);

   LHCb::LinksByKey operator()(const std::vector<LHCb::VPLightCluster>& clusters,
                               const LHCb::LinksByKey& digitLinks) const override; // < Algorithm execution

};

#endif
